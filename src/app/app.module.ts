import { LoginGuardService } from './services/login-guard.service';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from "./app.component";
import LoginModule from './layouts/auth/Login.module';
import HomeModule from './layouts/home/Home.module';
import { registerLocaleData } from '@angular/common';
import * as fr from '@angular/common/locales/fr';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AppLayoutModule } from './layouts/bolayouts/app.layout.module';
import { CountryService } from './components/bocomponents/service/country.service';
import { CustomerService } from './components/bocomponents/service/customer.service';
import { EventService } from './components/bocomponents/service/event.service';
import { IconService } from './components/bocomponents/service/icon.service';
import { NodeService } from './components/bocomponents/service/node.service';
import { PhotoService } from './components/bocomponents/service/photo.service';
import { ProductService } from './components/bocomponents/service/product.service';
import { ClientLayoutModule } from './layouts/clientlayouts/client-layout.module';
import { CodePasswordModule } from './components/employe/code-password/code-password.module';
import { NotfoundModule } from './components/bocomponents/notfound/notfound.module';
import { EditPasswordModule } from './components/employe/edit-password/edit-password.module';
import { LoginEmployeModule } from './components/employe/login-employe/login-employe.module';
import { ClientUserLayoutModule } from './layouts/clientuserlayouts/client-user-layout.module';
import { SignupModule } from './components/client/signup/signup.module';
import { LoginClientComponent } from './components/client/login-client/login-client.component';
import { LoginClientModule } from './components/client/login-client/login-client.module';
import { LoginAdminModule } from './components/admin/login-admin/login-admin.module';
@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LoginModule,
    HomeModule,
    AppRoutingModule,
    AppLayoutModule,
    ClientLayoutModule,
    ClientUserLayoutModule,
    NotfoundModule,
    CodePasswordModule,
    EditPasswordModule,
    LoginEmployeModule,
    SignupModule,
    LoginClientModule,
    LoginAdminModule
    

  ],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    CountryService,
    CustomerService,
    EventService,
    IconService,
    NodeService,
    PhotoService,
    ProductService,
    LoginGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    registerLocaleData(fr.default);
  }
 }
