import { Injectable } from '@angular/core';
import { Observable, catchError, of, tap, throwError } from 'rxjs';
import FetchHelper from '../helpers/FetchHelper';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Admin } from '../models/admin.model';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  login(username:string, password: string): Observable<any> {
    return this.http.post<any>(FetchHelper.setUrl(`api/admin/`), {username:username,password:password})
      .pipe(
        tap(_ => console.log("Auth admin")),
        catchError(this.handleError<any>('loginAdmin'))
      )
  }

  getAdminByToken(token: string): Observable<Admin> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `ADMIN`,
    });
    return this.http.get<Admin>(FetchHelper.setUrl('api/admin/byToken/'+token), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching admin")),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error('Error ',error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
