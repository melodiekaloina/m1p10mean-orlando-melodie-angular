import { EmployeService } from "src/app/services/employe.service";
import { Inject, Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class LoginGuardService {
  constructor(private employeService: EmployeService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const sessionString = localStorage.getItem("sessionEmp");
    if (sessionString == "" || sessionString == null) {
      this.router.navigate(["/employe/login"]);
      return false;
    } else {
      const tokenEmp = sessionString;
      return new Promise<boolean>((resolve, reject) => {
        this.employeService.getEmployeByToken(tokenEmp).subscribe((employe) => {
          if (!employe) {
            localStorage.removeItem("sessionEmp");
            this.router.navigate(["/employe/login"]);
            resolve(false);
          } else {
            resolve(true);
          }
        }, (error) => {
          localStorage.removeItem("sessionEmp");
          this.router.navigate(["/employe/login"]);
          resolve(false);
        });
      });
    }
  }
}
