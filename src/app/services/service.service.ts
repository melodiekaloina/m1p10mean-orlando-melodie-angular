import { Injectable } from '@angular/core';
import { Service } from '../models/service.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import FetchHelper from '../helpers/FetchHelper';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  services = [
    {
      servicename: 'Makeup',
      price: 60,
      commission: 10,
      duree: 60,
      _id: '1',
      description: 'Quisque volutpat non nisl id tincidunt. Praesent at eros vitae dui pulvinar ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diam lorem, commodo viverra metus mollis nec. Nam vehicula ipsum faucibus. Praesent at eros vitae dui pulvina the ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diamlorem, commodo viverra metus mollis vehicula ipsum faucibus.',
      image: 'assets/vendor/images/services/01.jpg'
    },
    {
      servicename: 'Hairdressing',
      price: 60,
      commission: 10,
      duree: 30,
      _id: '2',
      description: 'Quisque volutpat non nisl id tincidunt. Praesent at eros vitae dui pulvinar ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diam lorem, commodo viverra metus mollis nec. Nam vehicula ipsum faucibus. Praesent at eros vitae dui pulvina the ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diamlorem, commodo viverra metus mollis vehicula ipsum faucibus.',
      image: 'assets/vendor/images/services/02.jpg'
    },
    {
      servicename: 'Barber',
      price: 60,
      commission: 10,
      duree: 45,
      _id: '3',
      description: 'Quisque volutpat non nisl id tincidunt. Praesent at eros vitae dui pulvinar ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diam lorem, commodo viverra metus mollis nec. Nam vehicula ipsum faucibus. Praesent at eros vitae dui pulvina the ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diamlorem, commodo viverra metus mollis vehicula ipsum faucibus.',
      image: 'assets/vendor/images/services/03.jpg'
    },
    {
      servicename: 'Massage Theraphy',
      price: 60,
      commission: 10,
      duree: 30,
      _id: '4',
      description: 'Quisque volutpat non nisl id tincidunt. Praesent at eros vitae dui pulvinar ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diam lorem, commodo viverra metus mollis nec. Nam vehicula ipsum faucibus. Praesent at eros vitae dui pulvina the ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diamlorem, commodo viverra metus mollis vehicula ipsum faucibus.',
      image: 'assets/vendor/images/services/04.jpg'
    },
    {
      servicename: 'Body Treatments',
      price: 60,
      commission: 10,
      duree: 30,
      _id: '5',
      description: 'Quisque volutpat non nisl id tincidunt. Praesent at eros vitae dui pulvinar ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diam lorem, commodo viverra metus mollis nec. Nam vehicula ipsum faucibus. Praesent at eros vitae dui pulvina the ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diamlorem, commodo viverra metus mollis vehicula ipsum faucibus.',
      image: 'assets/vendor/images/services/05.jpg'
    },
    {
      servicename: 'Aromatherapy',
      price: 60,
      commission: 10,
      duree: 30,
      _id: '6',
      description: 'Quisque volutpat non nisl id tincidunt. Praesent at eros vitae dui pulvinar ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diam lorem, commodo viverra metus mollis nec. Nam vehicula ipsum faucibus. Praesent at eros vitae dui pulvina the ornare. Morbi mollis a enim nec ullamcorper. Proin condimentum ut mauris ut placerat. Donec commodo diamlorem, commodo viverra metus mollis vehicula ipsum faucibus.',
      image: 'assets/vendor/images/services/06.jpg'
    }

  ];


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json;multipart/form-data' })
  };

  constructor(private http: HttpClient) { }

  getAllService(token:string, side:string): Observable<Service[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    // return this.services;
    return this.http.get<Service[]>(FetchHelper.setUrl('api/admin/service'),{headers: headers})
      .pipe(
        tap(_ => console.log("Fetching services")),
        catchError(this.handleError<any>('getAllService'))
      )

  } 
  getAllServiceFree(): Observable<Service[]> {
    // return this.services;
    return this.http.get<Service[]>(FetchHelper.setUrl('api/admin/servicefree'))
      .pipe(
        tap(_ => console.log("Fetching services")),
        catchError(this.handleError<any>('getAllService'))
      )

  }

  getServiceById(serviceId: string, services: Service[]): Service {
    const service = services.find(service => service._id == serviceId);
    if (service) {

      return service;

    } else {

      throw new Error('Service not found!');

    }
  }

  // create
  addService(token:string, side:string,service: Service | undefined): Observable<Service> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.post<Service>(FetchHelper.setUrl("api/admin/new/service"), service, {headers:headers})
      .pipe(
        tap(_ => console.log("Saving service")),
        catchError(this.handleError<any>('addService'))
      )
  }


  // update 
  updateService(token:string, side:string,id: string, service: Service | undefined): Observable<Service> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.patch<Service>(FetchHelper.setUrl(`api/admin/update/service/${id}`), service, {headers:headers})
      .pipe(
        tap(_ => console.log("Updating service")),
        catchError(this.handleError<any>('updateService'))
      )
  }

  // delete 
  deleteService(token:string, side:string,id: string): Observable<Service> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.delete<Service>(FetchHelper.setUrl(`api/admin/delete/service/${id}`), {headers:headers})
      .pipe(
        tap(_ => console.log("Deleting service")),
        catchError(this.handleError<any>('deleteService'))
      )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
