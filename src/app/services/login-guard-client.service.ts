import { ClientService } from "src/app/services/client.service";
import { Inject, Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class LoginGuardClientService {
  constructor(private clientService: ClientService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const sessionString = localStorage.getItem("sessionClient");
    if (sessionString == "" || sessionString == null) {
      this.router.navigate(["/user/login"]);
      return false;
    } else {
      const tokenClient = sessionString;
      return new Promise<boolean>((resolve, reject) => {
        this.clientService.getClientByToken(tokenClient).subscribe((client) => {
          if (!client) {
            localStorage.removeItem("sessionClient");
            this.router.navigate(["/user/login"]);
            resolve(false);
          } else {
            resolve(true);
          }
        }, (error) => {
          localStorage.removeItem("sessionClient");
          this.router.navigate(["/user/login"]);
          resolve(false);
        });
      });
    }
  }
}
