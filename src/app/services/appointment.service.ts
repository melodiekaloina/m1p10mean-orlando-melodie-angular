import { Injectable } from "@angular/core";
import { Appointment, AppointmentItem } from "../models/appointment.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import FetchHelper from "../helpers/FetchHelper";
import { Observable, catchError, tap, throwError } from "rxjs";
import { AppointClient } from "../models/client.model";

@Injectable({
  providedIn: "root",
})
export class AppointmentService {
  appointments!: Appointment[];

  constructor(private http: HttpClient) {}

  getAllAppointment(): Promise<Appointment[]> {
    return this.http
      .get<any>("assets/demo/data/appointments.json")
      .toPromise()
      .then((res) => res.data as Appointment[])
      .then((data) => data);
  }

  addAppointment(token:string, side:string, appointment:Appointment): Observable<any>{
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.post<any>(FetchHelper.setUrl('api/client/new/appointment'),appointment, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching Addappointment")),
        catchError(error => {
          console.log('errrrror', error);
          return throwError(error);
        })
      );
  }

  getAppointmentById(appointmentId: string, appointments: Appointment[]): Appointment {
    const appointment = appointments.find(appointment => appointment._id == appointmentId);
    if (appointment) {

      return appointment; 

    } else {

      throw new Error('Appointment not found!');

    }
  }
  getAppointById(appointmentId: string, appointmentclients: AppointClient[]): Appointment {
    const appointment = appointmentclients.find(appointment => appointment._id!=null && appointment._id._id == appointmentId);
    if (appointment) {
      return appointment._id; 

    } else {

      throw new Error('Appointment not found!');

    }
  }
  getProgramById(programId: string, programs: AppointmentItem[]): AppointmentItem {
    const program = programs.find(program => program._id!=null && program._id == programId);
    if (program) {
      return program; 

    } else {

      throw new Error('Program not found!');

    }
  }

  getAppointmentsByIdClient(idClient:string):Promise<Appointment[]> {
    return this.http
    .get<any>("assets/demo/data/appointments.json")
    .toPromise()
    .then((res) => res.data as Appointment[])
    .then((data) => data);
  }

  getAppointmentsByIdEmploye(idEmploye:string):Promise<Appointment[]> {
    return this.http
    .get<any>("assets/demo/data/appointments.json")
    .toPromise()
    .then((res) => res.data as Appointment[])
    .then((data) => data);
  }


}
