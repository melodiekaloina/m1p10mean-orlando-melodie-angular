import { Injectable } from '@angular/core';
import { Employe, Workinghour } from '../models/employe.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import FetchHelper from '../helpers/FetchHelper';

@Injectable({
  providedIn: 'root'
})
export class EmployeService {

  employes: Employe[] = [];
  employe!: Employe;

  constructor(private http: HttpClient) { }

  getAllEmploye(token:string, side:string): Observable<Employe[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.post<Employe[]>(FetchHelper.setUrl('api/employee'),{},{headers: headers})
      .pipe(
        tap(_ => console.log("Fetching employes")), 
        catchError(this.handleError<any>('getAllEmploye'))
      )
  }
  getAllEmployeFree(): Observable<Employe[]> {
    // return this.employes;
    return this.http.post<Employe[]>(FetchHelper.setUrl('api/employee/free'),{})
      .pipe(
        tap(_ => console.log("Fetching employes")),
        catchError(this.handleError<any>('getAllEmploye'))
      )
  }

  getEmployeById(employeId: string, employes: Employe[]): Employe {
    const employe = employes.find(employe => employe._id == employeId);
    if (employe) {

      return employe; 

    } else {

      throw new Error('Employe not found!');

    }
  }
  getEmployesAvailable(token:string, side:string, idService:string, date:string){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    const data = {
      service: idService,
      date: date
    }
    return this.http.post<Employe[]>(FetchHelper.setUrl('api/employee/available'),data,{headers: headers})
      .pipe(
        tap(_ => console.log("Fetching employes available")), 
        catchError(this.handleError<any>('getEmployeAvailable'))
      )
  }
  getEmployeByToken(token: string): Observable<Employe> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `EMP`,
    });
    return this.http.get<Employe>(FetchHelper.setUrl('api/employee/byToken/'+token), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching employe")),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  // create
  addEmploye(token:string, side: string, employe: Employe | undefined): Observable<Employe> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    console.log('employe', employe);
    return this.http.post<Employe>(FetchHelper.setUrl("api/admin/new/employee"), employe, {headers:headers})
      .pipe(
        catchError(error => {
          return throwError(error);
        })
      )
  } 


  // update 
  updateEmploye(token:string, side:string, id: string, employe: Employe | undefined): Observable<Employe> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.put<Employe>(FetchHelper.setUrl(`api/employee/${id}`), employe, {headers:headers})
      .pipe(
        tap(_ => console.log("Updating employe")),
        catchError(error => {
          return throwError(error);
        })
      )
  }
  // update password
  updatePasswordEmploye(id: string, password:string): Observable<Employe> {
    return this.http.put<Employe>(FetchHelper.setUrl(`api/employee/updatePassword/${id}`), {"password":password})
      .pipe(
        tap(_ => console.log("Updating employe")),
        catchError(this.handleError<any>('updatePasswordEmploye'))
      )
  }

  // delete 
  deleteEmploye(token:string, side:string,id: string): Observable<Employe> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.delete<Employe>(FetchHelper.setUrl(`api/employee/delete${id}`), {headers:headers})
      .pipe(
        tap(_ => console.log("Deleting employe")),
        catchError(this.handleError<any>('deleteEmploye'))
      )
  }

  

  login(email:string, password: string): Observable<any> {
    return this.http.post<any>(FetchHelper.setUrl(`api/employee/authen`), {email:email,password:password})
      .pipe(
        tap(_ => console.log("Auth employe")),
        catchError(this.handleError<any>('loginEmploye'))
      )
  }

  getDefaultWorkingHour(): Promise<Workinghour[]>{
    return this.http
      .get<any>("assets/demo/data/defaultworkhour.json")
      .toPromise()
      .then((res) => res.data as Workinghour[])
      .then((data) => data);
  }

  updateWorkingHour(token:string, side:string, id: string, data: any | undefined): Observable<Employe> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.put<Employe>(FetchHelper.setUrl(`api/employee/${id}`), data, {headers:headers})
      .pipe(
        tap(_ => console.log("Updating working hour employe")),
        catchError(error => {
          return throwError(error);
        })
      )
  }

  getDetailEmploye(token: string, side: string, id: string): Observable<Employe> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.get<Employe>(FetchHelper.setUrl('api/employee/'+id), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching detail client")),
        catchError(error => {
          return throwError(error);
        })
      );
  }
  commission(token: string, side: string, id: string, date:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    const data = {
      "employee":id,
      "date":date
    }
    return this.http.post<any>(FetchHelper.setUrl('api/employee/mycommission'),data, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching mycommission")),
        catchError(error => {
          return throwError(error);
        })
      );
  }
  markAsDone(token: string, side: string, id: string, idprogram:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    const data = {
      employee: id,
      program: idprogram
    }
    return this.http.post<any>(FetchHelper.setUrl('api/employee/task/done'),data, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching marksAsDone")),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  addTask(token: string, side: string, idemploye: string, id:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    var data = {
      task: id,
    }
    return this.http.put<any>(FetchHelper.setUrl('api/employee/addtask/'+idemploye),data, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching add favorite")),
        catchError(error => {
          return throwError(error); 
        })
      );
  }
  removeTask(token: string, side: string, idemploye: string, id:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    var data = {
       task: id,
     }
    return this.http.post<any>(FetchHelper.setUrl('api/employee/removetask/'+idemploye),data, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching remove favorite")),
        catchError(error => {
          return throwError(error); 
        })
      );
  }

  

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error('Error ',error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
