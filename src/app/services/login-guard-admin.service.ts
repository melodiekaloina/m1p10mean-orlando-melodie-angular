import { AdminService } from "src/app/services/admin.service";
import { Inject, Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class LoginGuardAdminService {
  constructor(private adminService: AdminService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const sessionString = localStorage.getItem("sessionAdmin");
    if (sessionString == "" || sessionString == null) {
      this.router.navigate(["/admin/login"]);
      return false;
    } else {
      const tokenAdmin = sessionString;
      return new Promise<boolean>((resolve, reject) => {
        this.adminService.getAdminByToken(tokenAdmin).subscribe((admin) => {
          if (!admin) {
            localStorage.removeItem("sessionAdmin");
            this.router.navigate(["/admin/login"]);
            resolve(false);
          } else {
            resolve(true);
          }
        }, (error) => {
          localStorage.removeItem("sessionAdmin");
          this.router.navigate(["/admin/login"]);
          resolve(false);
        });
      });
    }
  }
}
