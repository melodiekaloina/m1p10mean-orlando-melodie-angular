import { Injectable } from '@angular/core';
import FetchHelper from '../helpers/FetchHelper';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, tap, catchError, throwError, of } from 'rxjs';
import { Spending, Spendingtype } from '../models/admin.model';

@Injectable({
  providedIn: 'root'
})
export class SpendingService {

  
  constructor(private http: HttpClient) { }
    
  // spending type
  getAllspendingtype(token:string, side:string): Observable<Spendingtype[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.get<Spendingtype[]>(FetchHelper.setUrl('api/spending-type'), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching spendingType")),
        catchError(error => {
          return throwError(error);
        })
      );
  }


  // create
  addSpendingtype(token:string, side:string,spendingtype: Spendingtype | undefined): Observable<Spendingtype> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    console.log('spendingtype', spendingtype);
    return this.http.post<Spendingtype>(FetchHelper.setUrl("api/spending-type"), spendingtype, {headers:headers})
      .pipe(
        catchError(error => {
          return throwError(error);
        })
      )
  }

  // update 
  updateSpendingtype(token:string, side:string,id: string, spendingtype: Spendingtype | undefined): Observable<Spendingtype> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.patch<Spendingtype>(FetchHelper.setUrl(`api/spending-type/${id}`), spendingtype, {headers:headers})
      .pipe(
        tap(_ => console.log("Updating spendingtype")),
        catchError(error => {
          return throwError(error);
        })
      )
  }
  
  // delete 
  deleteSpendingtype(token:string, side:string,id: string): Observable<Spendingtype> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.delete<Spendingtype>(FetchHelper.setUrl(`api/spending-type/${id}`), {headers:headers})
      .pipe(
        tap(_ => console.log("Deleting spendingtype")),
        catchError(error => {
          return throwError(error);
        })
      )
  }

// spending type
getAllspending(token:string, side:string): Observable<Spending[]> {
  const headers = new HttpHeaders({
    'Authorization': `Bearer ${token}`,
    'http_side': `${side}`,
  });
  return this.http.get<Spending[]>(FetchHelper.setUrl('api/spending'), {headers:headers})
    .pipe(
      tap(_ => console.log("Fetching spendingType")),
      catchError(error => {
        return throwError(error);
      })
    );
}


// create
addSpending(token:string, side:string,spending: Spending | undefined): Observable<Spending> {
  const headers = new HttpHeaders({
    'Authorization': `Bearer ${token}`,
    'http_side': `${side}`,
  });
  console.log('spending', spending);
  return this.http.post<Spending>(FetchHelper.setUrl("api/spending"), spending, {headers:headers})
    .pipe(
      catchError(error => {
        return throwError(error);
      })
    )
}

// update 
updateSpending(token:string, side:string,id: string, spending: Spending | undefined): Observable<Spending> {
  const headers = new HttpHeaders({
    'Authorization': `Bearer ${token}`,
    'http_side': `${side}`,
  });
  return this.http.patch<Spending>(FetchHelper.setUrl(`api/spending/${id}`), spending, {headers:headers})
    .pipe(
      tap(_ => console.log("Updating spending")),
      catchError(error => {
        return throwError(error);
      })
    )
}

// delete 
deleteSpending(token:string, side:string,id: string): Observable<Spending> {
  const headers = new HttpHeaders({
    'Authorization': `Bearer ${token}`,
    'http_side': `${side}`,
  });
  return this.http.delete<Spending>(FetchHelper.setUrl(`api/spending/${id}`), {headers:headers})
    .pipe(
      tap(_ => console.log("Deleting spending")),
      catchError(error => {
        return throwError(error);
      })
    )
}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error('Error ',error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getSpendingtypeById(spendingtypeId: string, spendingtypes: Spendingtype[]): Spendingtype {
    const spendingtype = spendingtypes.find(spendingtype => spendingtype._id == spendingtypeId);
    if (spendingtype) {

      return spendingtype; 

    } else {

      throw new Error('Spendingtype not found!');

    }
  }
}
