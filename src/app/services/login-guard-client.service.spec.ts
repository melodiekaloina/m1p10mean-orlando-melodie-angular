import { TestBed } from '@angular/core/testing';

import { LoginGuardClientService } from './login-guard-client.service';

describe('LoginGuardClientService', () => {
  let service: LoginGuardClientService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoginGuardClientService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
