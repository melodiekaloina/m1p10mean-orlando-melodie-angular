import { Injectable } from '@angular/core';
import { Observable, catchError, tap, throwError } from 'rxjs';
import FetchHelper from '../helpers/FetchHelper';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Appointment, Offer } from '../models/appointment.model';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  constructor(private http: HttpClient) { }

  getAllOffer(token:string, side:string): Observable<Offer[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.get<Offer[]>(FetchHelper.setUrl('api/admin/offer'),{headers: headers})
      .pipe(
        tap(_ => console.log("Fetching offers")), 
        catchError(error => {
          return throwError(error);
        })
      )
  }
  takeOffer(token:string, side:string, id: string, appoint: Appointment): Observable<Offer[]> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    const data = {
      specialoffer: id,
      appointment: appoint
    }
    console.log(data);
    // return new Observable<Offer[]>;
    return this.http.post<Offer[]>(FetchHelper.setUrl('api/client/new/appointment/offer'),data,{headers: headers})
      .pipe(
        tap(_ => console.log("Fetching offers")), 
        catchError(error => {
          return throwError(error);
        })
      )
  }

  addOffer(token:string, side:string, offer:Offer): Observable<any>{
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.post<any>(FetchHelper.setUrl('api/admin/new/offer'),offer, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching Addoffer")),
        catchError(error => {
          return throwError(error);
        })
      );
  }

  getOfferById(offerId: string, offers: Offer[]): Offer {
    const offer = offers.find(offer => offer._id == offerId);
    if (offer) {

      return offer; 

    } else {

      throw new Error('Offer not found!');

    }
  }

}
