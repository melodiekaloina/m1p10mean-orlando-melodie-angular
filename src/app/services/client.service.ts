import { Injectable } from "@angular/core";
import { ClientComment } from "../models/client-comment.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Client } from "../models/client.model";
import { Observable, catchError, of, tap, throwError } from "rxjs";
import FetchHelper from "../helpers/FetchHelper";

@Injectable({
  providedIn: "root",
})
export class ClientService {
  clients!: Client[];

  constructor(private http: HttpClient) {}

  login(email:string, password: string): Observable<any> {
    return this.http.post<any>(FetchHelper.setUrl(`api/client/authen`), {email:email,password:password})
      .pipe(
        tap(_ => console.log("Auth client")),
        catchError(this.handleError<any>('loginClient'))
      )
  }

  getAllClient(): Promise<Client[]> {
    return this.http
      .get<any>("assets/demo/data/clients.json")
      .toPromise()
      .then((res) => res.data as Client[])
      .then((data) => data);
  }

  getClientById(clientId: string, clients: Client[]): Client {
    const client = clients.find(client => client._id == clientId);
    if (client) {

      return client; 

    } else {

      throw new Error('Client not found!');

    }
  }
  getClientByToken(token: string): Observable<Client> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `CLIENT`,
    });
    return this.http.get<Client>(FetchHelper.setUrl('api/client/byToken/'+token), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching client")),
        catchError(error => {
          return throwError(error);
        })
      );
  }
  getDetailClient(token: string, side: string, id: string): Observable<Client> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.get<Client>(FetchHelper.setUrl('api/client/'+id), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching detail client")),
        catchError(error => {
          return throwError(error);
        })
      );
  }
  addClient(client: Client | undefined): Observable<Client> {
    console.log('client', client);
    return this.http.post<Client>(FetchHelper.setUrl("api/client/new"), client)
      .pipe(
        catchError(error => {
          return throwError(error); 
        })
      )
  }

  pay(token: string, side: string, id: string, idappoint:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    const data = {
      client: id,
      appointment: idappoint
    }
    return this.http.post<any>(FetchHelper.setUrl('api/client/pay'),data, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching pay")),
        catchError(error => {
          return throwError(error); 
        })
      );
  }
  addFavorite(token: string, side: string, idclient: string, title: string, id:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    var data = {};
    if(title == 'service'){
      data = {
       service: id,
     }
    }
    if(title == 'employee'){
      data = {
       employee: id,
     }
    }
    return this.http.post<any>(FetchHelper.setUrl('api/client/favorite/'+idclient),data, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching add favorite")),
        catchError(error => {
          return throwError(error); 
        })
      );
  }
  removeFavorite(token: string, side: string, idclient: string, title: string, id:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    var data = {};
    if(title == 'service'){
      data = {
       service: id,
     }
    }
    if(title == 'employee'){
      data = {
       employee: id,
     }
    }
    return this.http.post<any>(FetchHelper.setUrl('api/client/removefavorite/'+idclient),data, {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching remove favorite")),
        catchError(error => {
          return throwError(error); 
        })
      );
  }

  getAllClientComment(): ClientComment[] {
    return [
      new ClientComment(
        "Marie Dupont",
        "Je suis une habituée du salon depuis des années et je ne pourrais pas être plus satisfaite ! Les esthéticiennes sont incroyablement compétentes et attentionnées. "
      ),
      new ClientComment(
        "Sophie Martin",
        "Je viens de découvrir ce salon et je suis déjà conquise ! L'équipe est tellement accueillante et professionnelle."
      ),
      new ClientComment(
        "Pierre Lambert",
        "L'ambiance est chaleureuse, le personnel est compétent et sympathique, et le résultat dépasse toujours mes attentes. "
      ),
    ];
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error('Error ',error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
