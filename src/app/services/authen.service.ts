import { Injectable } from '@angular/core';
import { Employe } from '../models/employe.model';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import FetchHelper from '../helpers/FetchHelper';

@Injectable({
    providedIn: 'root'
})
export class AuthenService {


    constructor(private http: HttpClient) { }



    // sendEmail
    sendEmail(employe: Employe): Observable<any> {
         return this.http.post<any>(FetchHelper.setUrl("api/mail/init"), {email:employe.email})
            .pipe(
                tap(_ => console.log("sending email")),
                catchError(this.handleError<any>('sendEmail'))
            )
    }    

    // check
    isValid(email:string, token:number): Observable<any> {
        const json = {email:email, token:token};
        console.log(json);
        return this.http.post<any>(FetchHelper.setUrl("api/mail/valid"), json)
            .pipe(
                tap(_ => console.log("code valid")),
                catchError(error=>{
                    return throwError(error);
                })
            ) 
    }   
    


    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}
