import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, tap, throwError } from 'rxjs';
import FetchHelper from '../helpers/FetchHelper';
import { Earning, Reservation } from '../models/stat.model';

@Injectable({
  providedIn: 'root'
})
export class StatService {

  constructor(private http: HttpClient) { }

  getReservation(token: string, side:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.get<any>(FetchHelper.setUrl('api/admin/reservation/?day=true&month=true&year=true'), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching reservation")),
        catchError(error => {
          return throwError(error);
        })
      );
  }
  getEarning(token: string, side:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.get<any>(FetchHelper.setUrl('api/admin/earning/?day=true&month=true&year=true'), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching earning")),
        catchError(error => {
          return throwError(error);
        })
      );
  }
  getBenefit(token: string, side:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.get<any>(FetchHelper.setUrl('api/admin/benefit'), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching earning")),
        catchError(error => {
          return throwError(error);
        })
      );
  }
  getAvgwork(token: string, side:string): Observable<any> {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`,
      'http_side': `${side}`,
    });
    return this.http.get<any>(FetchHelper.setUrl('api/admin/employee/avgwork'), {headers:headers})
      .pipe(
        tap(_ => console.log("Fetching earning")),
        catchError(error => {
          return throwError(error);
        })
      );
  }
}
