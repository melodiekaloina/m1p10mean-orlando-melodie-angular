import * as CryptoJS from 'crypto-js';

export function stringToMD5(input: string): string {
    return CryptoJS.MD5(input).toString();
}

export function stringToSha1(input: string): string {
    return CryptoJS.SHA1(input).toString();
}

export const side={
    CLIENT:"CLIENT",
    ADMIN: "ADMIN",
    EMP: "EMP",
  }

  export const appointmentState = {
    CREATED: "CREATED",
    UPCOMING: "UPCOMING",
    ONGOING: "ONGOING",
    CANCELED: "CANCELED",
    DONE: "DONE",
    PAID: "PAID",
  }