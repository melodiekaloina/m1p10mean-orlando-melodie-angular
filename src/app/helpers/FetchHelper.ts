import { environment } from "../../environments/environment";

export default class FetchHelper{

  static setUrl(url:string){
      return environment.domain+url;
  }
  


  static sendMethodGet=async (url:string)=>{
    const response=await fetch(FetchHelper.setUrl(url),{
        method:"GET"
    });
    const data=await response.json();
    return data;
  }

  static sendMethodPost=async (url:string,donnee:URLSearchParams)=>{
    const response=await fetch(FetchHelper.setUrl(url),{
        method:"POST",
        body:donnee
    });
    const data=await response.json();
    return data;
  }

  static sendMethodPostFormData=async (url:string,donnee:FormData)=>{
    const response=await fetch(FetchHelper.setUrl(url),{
        method:"POST",
        body:JSON.stringify(donnee)
    });
    const data=await response.json();
    return data;
  }

}
