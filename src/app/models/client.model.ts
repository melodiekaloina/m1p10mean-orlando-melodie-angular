import { Appointment } from "./appointment.model";

export interface Client {
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  phoneNumber?: string;
  _id?: string;
  preferences?: {
    serviceReview?: string[];
    employeeReview?: string[];
  },
  appointments?: AppointClient[]
}

export interface AppointClient {
  date: string;
  state: string;
  _id: Appointment;
}
