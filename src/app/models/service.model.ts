
export interface Service {
    servicename?: string,
    price?: number,
    commission?: number,
    duree?: number,
    _id?: string,
    description?: string,
    photo?: string

}