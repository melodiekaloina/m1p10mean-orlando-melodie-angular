import { AppointmentItem } from 'src/app/models/appointment.model';

export interface Employe {
    firstName?: string,
    lastName?: string,
    cin?: string,
    datebirth?: string | Date,
    address?: string,
    email?: string,
    salary?: string,
    hired_at?: string | Date,
    _id?: string,
    password?:string,
    tasks?: string[],
    workhour?: Workinghour[],
    schedule?: AppointmentItem[]


}


export interface Workinghour{
    dayofweek: number,
    times: string[][],
    dates: Date[][],
    _id?: string
}