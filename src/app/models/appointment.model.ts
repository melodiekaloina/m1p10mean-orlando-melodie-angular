import { Client } from "./client.model";
import { Employe } from "./employe.model";
import { Service } from "./service.model";

export interface AppointmentItem {
  employee?: string;
  service?: string;
  employeobj?: Employe;
  serviceobj?: Service;
  start?: string;
  end?: string;
  price?: number;
  state?: string;
  _id?: string
}

export interface Appointment {
  client?: string;
  employe?: string;
  service?: string;
  date?: string;
  state?: string;
  _id?: string;
  programs?: AppointmentItem[];
  price?: number;
  commission?: number;
  clientobj?: Client;
  employeobj?: Employe;
  serviceobj?: Service;
}


export interface ServiceOffer {
  service?: string,
  serviceobj?: Service,
  reduction?: number,
  _id?: string
}
export interface Offer{
_id?: string,
offername?: string,
reduction?: number,
services?: ServiceOffer[],
start?: string,
end?: string
}