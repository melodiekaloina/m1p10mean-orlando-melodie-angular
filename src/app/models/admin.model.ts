
export interface Admin {
    username?: string,
    password?:string,
    _id?:string
}

export interface Spendingtype {
    spendinglabel?: string,
    spendingvalue?: number,
    code?: string,
    _id?: string
}

export interface Spending{
    sptype?: string,
    amount?: number,
    date?: string,
    _id?: string,
    sptypeobj?: Spendingtype
}