
export class ClientComment {

    constructor(
        public clientname: string,
        public comment: string) {

    }

}