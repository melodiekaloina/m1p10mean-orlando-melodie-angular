export interface Reservation {
  totalReservation?: number;
  day?: number;
  month?: number;
  year?: number;
}

export interface Earning {
  totalEarning?: number;
  day?: number;
  month?: number;
  year?: number;
}

export interface Benefit {
  month?: string;
  key?: number;
  totalEarning?: number;
  totalSpending?: number;
  benefit?: number;
}

export interface Avgwork {
  _id?: string;
  firstName?: string;
  lastName?: string;
  averageWorkingHours?: number;
}

