import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BocomponentsRoutingModule } from './bocomponents-routing.module';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        BocomponentsRoutingModule
    ]
})
export class BocomponentsModule { }
