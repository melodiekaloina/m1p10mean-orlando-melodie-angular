import { side } from './../../../helpers/utils';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Service } from 'src/app/models/service.model';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-crud-service',
  templateUrl: './crud-service.component.html',
  styleUrls: ['./crud-service.component.css'],
  
})
export class CrudServiceComponent implements OnInit {
  tokenAdmin!:string;
  services!: Service[];
  service!: Service;
  loading: boolean = true;

  serviceDialog: boolean = false;

  deleteServiceDialog: boolean = false;

  submitted: boolean = false;

  cols: any[] = [];
  
  globalFilterFields: any[] = [];

  rowsPerPageOptions = [10, 20, 30];

  errorField!: any[]; 


  constructor(private serviceService: ServiceService, private messageService: MessageService) { }

  ngOnInit() {
    this.tokenAdmin = localStorage.getItem('sessionAdmin')||'';

    this.errorField = [
      { field: "servicename", header: "Nom" },
      { field: "price", header: "Prix" },
      { field: "commission", header: "Commission" },
      { field: 'duree', header: "Duree" },
    ];

    this.cols = [
      { field: 'servicename', header: 'Nom' },
      { field: 'price', header: 'Prix' },
      { field: 'commission', header: 'Commission' },
      { field: 'duree', header: 'Duree' },
      // { field: 'image', header: 'Image' }
    ];
  
    this.globalFilterFields = ['servicename'];
    this.serviceService.getAllService(this.tokenAdmin, side.ADMIN).subscribe(services => {      
      this.services = services
      this.loading = false;
    })
  }

  hideDialog() {
    this.initValue();
    this.serviceDialog = false;
    this.submitted = false;
  }

  openNew() {
    this.service = {};
    this.submitted = false;
    this.serviceDialog = true;
  }

  editService(service: Service) {
    this.service = { ...service };
    this.serviceDialog = true;
  }

  deleteService(service: Service) {
    this.deleteServiceDialog = true;
    this.service = { ...service };
  }

  confirmDelete() {
    this.deleteServiceDialog = false;
    this.serviceService.deleteService(this.tokenAdmin, side.ADMIN,this.service._id||'').subscribe(serviceItem => {

      this.services = this.services.filter(val => val._id != this.service._id);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Service Deleted', life: 3000 });
      this.service = {};
    });
    }

  saveService() {
    this.initValue();
    this.submitted = true;

    var requiredInvalid = false;
    this.errorField.forEach(field => {
      const fieldValue = (this.service as any)[field.field];
      if (fieldValue == undefined || fieldValue == '') {
        this.setErrorForField(field.field, field.header + ' est requis.');
        requiredInvalid = true;
      }
    });
    if (requiredInvalid ) {
      return;
    }

    if (this.service.servicename?.trim()) {
      // update
      if (this.service._id) {
        // @ts-ignore
        this.serviceService.updateService(this.tokenAdmin, side.ADMIN,this.service._id, this.service).subscribe(serviceItem => 
          {
            this.services[this.findIndexById(serviceItem._id||'')] = serviceItem;
        });
        

        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Service Updated', life: 3000 });

      }  
      // create 
      else {
        // this.service._id = this.createId();
        // @ts-ignore
        this.serviceService.addService(this.service).subscribe(serviceItem => this.services.push(serviceItem));
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Service Created', life: 3000 });
      }

      this.services = [...this.services];
      this.serviceDialog = false;
      this.service = {};
    }
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.services.length; i++) {
      if (this.services[i]._id === id) {
        index = i;
        break;
      }
    }

    return index;
  }

  createId(): string {
    let id = '';
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

  onFileSelected(event: any): void {
    const file: File = event.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = () => {
        if (typeof reader.result === 'string'){
          this.service.photo = reader.result;

        }
      };
      reader.readAsDataURL(file);
    }
  }

  getErrorByFieldName(fieldName: string) {
    return this.errorField.find(field => field.field === fieldName);
  }

  setErrorForField(fieldName: string, errorMessage: string | undefined): void {
    const fieldObj = this.errorField.find(field => field.field === fieldName);
    if (fieldObj) {
      fieldObj.error = errorMessage;
    }
  }

  initValue() {
    //init errorField
    this.errorField.forEach(field => {
      this.setErrorForField(field.field, undefined);
    });
  }
}
