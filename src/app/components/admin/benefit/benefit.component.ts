import { Component } from "@angular/core";
import { side } from "src/app/helpers/utils";
import { Benefit } from "src/app/models/stat.model";
import { StatService } from "src/app/services/stat.service";

@Component({
  selector: "app-benefit",
  templateUrl: "./benefit.component.html",
  styleUrls: ["./benefit.component.css"],
})
export class BenefitComponent {
  tokenAdmin!: string;

  benefits!: Benefit[];
  selectedbenefit: Benefit| undefined ;

  date: Date = new Date();
  monthItems: any[] = [];
  selectedMonth!: any;

  constructor(private statService: StatService) {}

  ngOnInit() {
    this.tokenAdmin = localStorage.getItem("sessionAdmin") || "";
    this.statService
      .getBenefit(this.tokenAdmin, side.ADMIN)
      .subscribe((benefits) => {
        this.benefits = benefits.data;
        
        this.monthItems = [
          { label: "Janvier", value: 1 },
          { label: "Fevrier", value: 2 },
          { label: "Mars", value: 3 },
          { label: "Avril", value: 4 },
          { label: "Mai", value: 5 },
          { label: "Juin", value: 6 },
          { label: "Juillet", value: 7 },
          { label: "Aout", value: 8 },
          { label: "Septembre", value: 9 },
          { label: "Octobre", value: 10 },
          { label: "Novembre", value: 11 },
          { label: "Decembre", value: 12 },
        ];
        // this.selectedMonth = this.monthItems.find(
        //   (item) => item.value == new Date().getMonth()
        // );
        this.selectedMonth = new Date().getMonth()+1;
        this.selectedbenefit = this.benefits.find(item => item.key == this.selectedMonth);
      });
  }

  onMonthSelect(value: any) {
    this.selectedMonth = value;
        this.selectedbenefit = this.benefits.find(item => item.key == this.selectedMonth);
  }
}
