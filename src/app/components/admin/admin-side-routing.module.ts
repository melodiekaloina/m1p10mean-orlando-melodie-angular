
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CrudEmployeComponent } from './crud-employe/crud-employe.component';
import { CrudServiceComponent } from './crud-service/crud-service.component';
import { AboutEmployeComponent } from './about-employe/about-employe.component';
import { CrudSpendingtypeComponent } from './crud-spendingtype/crud-spendingtype.component';
import { CrudSpendingComponent } from './crud-spending/crud-spending.component';
import { ReservationComponent } from './reservation/reservation.component';
import { EarningComponent } from './earning/earning.component';
import { BenefitComponent } from './benefit/benefit.component';
import { AvgworkComponent } from './avgwork/avgwork.component';
import { CreateOffreComponent } from './create-offre/create-offre.component';
import { ListOffreAdminComponent } from './list-offre-admin/list-offre-admin.component';

@NgModule({
    imports: [RouterModule.forChild([
        // { path: '', loadChildren: () => import('./home-page-admin/home-page-admin.module').then(m => m.HomePageAdminModule) }, 
        { path: "crud/employe", component: CrudEmployeComponent },
        { path: "crud/service", component: CrudServiceComponent },
        { path: "crud/spendingtype", component: CrudSpendingtypeComponent },
        { path: "crud/spending", component: CrudSpendingComponent },
        { path: "list-offre", component: ListOffreAdminComponent },
        { path: "create-offre", component: CreateOffreComponent },
        { path: "about-employe/:id", component: AboutEmployeComponent },
        { path: "", component: ReservationComponent },
        { path: "earning", component: EarningComponent },
        { path: "benefit", component: BenefitComponent },
        { path: "avgwork", component: AvgworkComponent },
        // { path: '**', redirectTo: '/notfound' }
    ])], 
    exports: [RouterModule]
})
export class AdminSideRoutingModule { }
