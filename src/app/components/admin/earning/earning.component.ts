import { Component } from "@angular/core";
import { side } from "src/app/helpers/utils";
import { Earning } from "src/app/models/stat.model";
import { StatService } from "src/app/services/stat.service";


@Component({
  selector: "app-earning",
  templateUrl: "./earning.component.html",
  styleUrls: ["./earning.component.css"],
})
export class EarningComponent {
  tokenAdmin!: string;
  earnings: Earning[] = [  ];
  lineOptions: any;
  linejData: any;
  linemData: any;

  // filtre mois j
  monthFilterj!: number;

  // filtre annee
  yearFilterj!: number;

  labelj: any[] = [];
  dataj: any[] = [];
  
  datej: Date = new Date();

  labelm: any[] = [];
  datam: any[] = [];

  // filtre annee
  yearFilterm!: number;
  datem: Date = new Date();

  selectedFilteroption: any = null;

    filteroptions: any[] = [
        { name: 'Par jour', key: 0 },
        { name: 'Par mois', key: 1 },
    ];

    constructor(
      private statService: StatService
    ) {}

  ngOnInit() {
    this.tokenAdmin = localStorage.getItem('sessionAdmin')||'';
    this.selectedFilteroption = this.filteroptions[0];

    this.statService
      .getEarning(this.tokenAdmin, side.ADMIN)
      .subscribe((earnings) => {
        this.earnings = earnings.data;
        
        this.monthFilterj = this.datej.getMonth() + 1;
        this.yearFilterj = this.datej.getFullYear();
        this.yearFilterm = this.datem.getFullYear();
        this.filterLinejData();
        this.filterLinemData();
        this.initCharts();
      });   
  }

  filterLinejData() {
    // generate the label and data
    // filter
    const data: Earning[] = this.earnings.filter(
      (res) => res.month == this.monthFilterj && res.year == this.yearFilterj
    );

    data.sort((a: any, b: any) => a.day - b.day);
    this.labelj = [];
    this.dataj = [];
    data.forEach((element) => {
      this.labelj.push(element.day);
      this.dataj.push(element.totalEarning);
    });
  }
  filterLinemData() {
    // generate the label and data
    const monthItems = [
      "Janvier",
      "Fevrier",
      "Mars",
      "Avril",
      "Mai",
      "Juin",
      "Juillet",
      "Aout",
      "Septembre",
      "Octobre",
      "Novembre",
      "Decembre",
    ];

    const data: Earning[] = this.earnings.filter(
      (res) => res.year == this.yearFilterm
      );


    this.labelm = [];
    this.datam = [];
    const currentMonth = new Date().getMonth() + 1;
    for(let i=0;i<12;i++){
      this.labelm.push(monthItems[i]);
      var totalEarningMonth = 0;
      data.forEach(res => {if(res.month == i+1){ totalEarningMonth+= res.totalEarning||0}});
      this.datam.push(totalEarningMonth);
    }

  }

  onMonthjSelect(value: any) {
    this.monthFilterj = value.getMonth() + 1;
    this.yearFilterj = value.getFullYear();
    this.filterLinejData();
    this.linejData.labels = this.labelj;
    this.linejData.datasets[0].data = this.dataj;
    this.linejData = { ...this.linejData };
  }

  onYearmSelect(value: any) {
    this.yearFilterm = value.getFullYear();
    this.filterLinemData();
    this.linemData.labels = this.labelm;
    this.linemData.datasets[0].data = this.datam;
    this.linemData = { ...this.linemData };

  }
  initCharts() {
    const documentStyle = getComputedStyle(document.documentElement);
    const textColor = documentStyle.getPropertyValue("--text-color");
    const textColorSecondary = documentStyle.getPropertyValue(
      "--text-color-secondary"
    );
    const surfaceBorder = documentStyle.getPropertyValue("--surface-border");
    this.linejData = {
      labels: this.labelj,
      datasets: [
        {
          label: "earning",
          data: this.dataj,
          fill: false,
          backgroundColor: documentStyle.getPropertyValue("--primary-500"),
          borderColor: documentStyle.getPropertyValue("--primary-500"),
          tension: 0.4,
        },
      ],
    };
    this.linemData = {
      labels: this.labelm,
      datasets: [
        {
          label: "earning",
          data: this.datam,
          fill: false,
          backgroundColor: documentStyle.getPropertyValue("--primary-500"),
          borderColor: documentStyle.getPropertyValue("--primary-500"),
          tension: 0.4,
        },
      ],
    };

    this.lineOptions = {
      plugins: {
        legend: {
          labels: {
            fontColor: textColor,
          },
        },
      },
      scales: {
        x: {
          ticks: {
            color: textColorSecondary,
          },
          grid: {
            color: surfaceBorder,
            drawBorder: false,
          },
        },
        y: {
          ticks: {
            color: textColorSecondary,
          },
          grid: {
            color: surfaceBorder,
            drawBorder: false,
          },
        },
      },
    };
  }

}
