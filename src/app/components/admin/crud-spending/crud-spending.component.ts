import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { Table } from "primeng/table";
import { side } from "src/app/helpers/utils";
import { Spending, Spendingtype } from "src/app/models/admin.model";
import { AdminService } from "src/app/services/admin.service";
import { SpendingService } from "src/app/services/spending.service";

@Component({
  selector: "app-crud-spending",
  templateUrl: "./crud-spending.component.html",
  styleUrls: ["./crud-spending.component.css"],
})
export class CrudSpendingComponent {
  tokenAdmin!: string;
  spendings!: Spending[];
  spending!: Spending;
  loading: boolean = true;
  spendingtypes!: Spendingtype[];

  spendingtypeItems: any[] = [];

  spendingDialog: boolean = false;

  deleteSpendingDialog: boolean = false;

  submitted: boolean = false;

  cols: any[] = [];

  globalFilterFields: any[] = [];

  rowsPerPageOptions = [10, 20, 30];

  errorField!: any[];

  dateModel: Date = new Date();

  constructor(
    private adminService: AdminService,
    private messageService: MessageService,
    private router: Router,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private spendingService: SpendingService
  ) {}

  // required: cin, firstname, lastname, address, datebirth
  ngOnInit() {
    this.tokenAdmin = localStorage.getItem("sessionAdmin") || "";
    this.errorField = [
      { field: "sptype", header: "Type Depense" },
      { field: "date", header: "Date" },
    ];

    this.cols = [
      { field: "sptype", header: "Type depense" },
      { field: "amount", header: "Montant" },
      { field: "date", header: "Date" },
    ];

    this.globalFilterFields = ["spendinglabel", "spendingvalue", "code"];
    this.spendingService
      .getAllspendingtype(this.tokenAdmin, side.ADMIN)
      .subscribe((spendingtypes) => {
        this.spendingtypes = spendingtypes;
        this.spendingtypes.forEach((spendingtype) => {
          this.spendingtypeItems.push({
            label: spendingtype.spendinglabel,
            value: spendingtype._id,
          });
        });

        this.spendingService
          .getAllspending(this.tokenAdmin, side.ADMIN)
          .subscribe((spendings) => {
            spendings.forEach((spending: Spending) => {
              spending.sptypeobj = this.spendingService.getSpendingtypeById(
                spending.sptype || "",
                this.spendingtypes
              );
            });
            this.spendings = spendings;
          });
          this.loading = false;
      });
  }

  hideDialog() {
    this.initValue();
    this.spendingDialog = false;
    this.submitted = false;
  }

  openNew() {
    this.spending = {};
    this.dateModel = new Date();
    this.spending.date = this.todayDateStr();
    this.submitted = false;
    this.spendingDialog = true;
  }

  editSpending(spending: Spending) {
    this.spending = { ...spending };
    this.dateModel = new Date(spending.date || "");
    this.spendingDialog = true;
  }

  deleteSpending(spending: Spending) {
    this.deleteSpendingDialog = true;
    this.spending = { ...spending };
  }

  confirmDelete() {
    this.deleteSpendingDialog = false;
    // check if deletable
    this.spendingService
      .deleteSpending(this.tokenAdmin, side.ADMIN, this.spending._id || "")
      .subscribe(
        (spendingItem) => {
          this.spendings = this.spendings.filter(
            (val) => val._id !== this.spending._id
          );
          this.messageService.add({
            severity: "success",
            summary: "Successful",
            detail: "Spending Deleted",
            life: 3000,
          });
          this.spending = {};
        },
        (error) => {
          this.messageService.add({
            severity: "error",
            summary: "Error",
            detail: error.error.data.message,
            life: 3000,
          });
        }
      );
  }

  saveSpending() {
    this.submitted = true;
    this.initValue();
    //control

    var requiredInvalid = false;
    this.errorField.forEach((field) => {
      const fieldValue = (this.spending as any)[field.field];
      if (fieldValue == undefined || fieldValue == "") {
        this.setErrorForField(field.field, field.header + " est requis.");
        requiredInvalid = true;
      }
    });
    if (requiredInvalid) {
      return;
    }

    this.spending.amount = !this.spending.amount ? 0 : this.spending.amount;

    // update
    if (this.spending._id) {
      this.spendingService
        .updateSpending(
          this.tokenAdmin,
          side.ADMIN,
          this.spending._id,
          this.spending
        )
        .subscribe(
          (spendingItem) => {
            spendingItem.sptypeobj = this.spendingService.getSpendingtypeById(
              spendingItem.sptype || "",
              this.spendingtypes
            );
            this.spendings[this.findIndexById(spendingItem._id || "")] =
              spendingItem;
            this.messageService.add({
              severity: "success",
              summary: "Successful",
              detail: "Spending Updated",
              life: 3000,
            });
          },
          (error) => {
            // if (error.status === 500) {
            //   this.messageService.add({ severity: 'error', summary: 'Error', detail: error.message, life: 3000 });
            // }
          }
        );
    }
    // create
    else {
      this.spendingService
        .addSpending(this.tokenAdmin, side.ADMIN, this.spending)
        .subscribe(
          (spendingItem) => {
            spendingItem.sptypeobj = this.spendingService.getSpendingtypeById(
              spendingItem.sptype || "",
              this.spendingtypes
            );
            this.spendings.push(spendingItem);
            this.messageService.add({
              severity: "success",
              summary: "Successful",
              detail: "Spending Created",
              life: 3000,
            });
          },
          (error) => {
            // if (error.status === 401) {
            //   this.messageService.add({ severity: 'error', summary: 'Error', detail: error.message, life: 3000 });
            // }
          }
        );
    }

    this.spendings = [...this.spendings];
    this.spendingDialog = false;
    this.spending = {};
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.spendings.length; i++) {
      if (this.spendings[i]._id === id) {
        index = i;
        break;
      }
    }

    return index;
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }

  initValue() {
    //init errorField
    this.errorField.forEach((field) => {
      this.setErrorForField(field.field, undefined);
    });
  }

  getErrorByFieldName(fieldName: string) {
    return this.errorField.find((field) => field.field === fieldName);
  }

  setErrorForField(fieldName: string, errorMessage: string | undefined): void {
    const fieldObj = this.errorField.find((field) => field.field === fieldName);
    if (fieldObj) {
      fieldObj.error = errorMessage;
    }
  }

  onDateSelect(value: any) {
    this.spending.date = this.formatDateString(value);
  }
  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-dd")!;
  }
  formatDate(date: string) {
    return new Date(date);
  }
  todayDateStr() {
    let today = new Date();

    let year = today.getFullYear().toString();
    let month = (today.getMonth() + 1).toString().padStart(2, "0");
    let day = today.getDate().toString().padStart(2, "0");

    let dateString = `${year}-${month}-${day}`;
    return dateString;
  }
}
