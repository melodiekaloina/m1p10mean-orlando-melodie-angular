import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudSpendingComponent } from './crud-spending.component';

describe('CrudSpendingComponent', () => {
  let component: CrudSpendingComponent;
  let fixture: ComponentFixture<CrudSpendingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CrudSpendingComponent]
    });
    fixture = TestBed.createComponent(CrudSpendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
