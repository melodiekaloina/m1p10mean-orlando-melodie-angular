import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOffreAdminComponent } from './list-offre-admin.component';

describe('ListOffreAdminComponent', () => {
  let component: ListOffreAdminComponent;
  let fixture: ComponentFixture<ListOffreAdminComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListOffreAdminComponent]
    });
    fixture = TestBed.createComponent(ListOffreAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
