import { Component } from "@angular/core";
import { Table } from "primeng/table";
import { side } from "src/app/helpers/utils";
import { Employe } from "src/app/models/employe.model";
import { Avgwork } from "src/app/models/stat.model";
import { EmployeService } from "src/app/services/employe.service";
import { StatService } from "src/app/services/stat.service";

@Component({
  selector: "app-avgwork",
  templateUrl: "./avgwork.component.html",
  styleUrls: ["./avgwork.component.css"],
})
export class AvgworkComponent {
  tokenAdmin!: string;
  datatable: Avgwork[] = [];
  employes!: Employe[];
  avgworks!: Avgwork[];
  loading:boolean = true;

  cols: any[] = [];

  globalFilterFields: any[] = [];

  rowsPerPageOptions = [10, 20, 30];

  constructor(
    private statService: StatService,
    private employeService: EmployeService
  ) {}

  ngOnInit() {
    this.cols = [
      { field: 'firstName', header: 'Nom' },
      { field: 'lastName', header: 'Prenom' },
      { field: 'averageWorkingHours', header: 'Duree moyen ' },
    ];

    this.globalFilterFields = ['firstName', 'lastName', 'averageWorkingHours'];

    this.tokenAdmin = localStorage.getItem("sessionAdmin") || "";

    this.statService
      .getAvgwork(this.tokenAdmin, side.ADMIN)
      .subscribe((avgworks) => {
        this.avgworks = [];
        (avgworks.data as any[]).forEach(element => {
          this.avgworks.push({
            _id: element._id._id,
            firstName: element._id.firstName,
            lastName: element._id.lastName,
            averageWorkingHours: element.averageWorkingHours,
          });
        });
        console.log(this.avgworks);

        this.employeService
          .getAllEmploye(this.tokenAdmin, side.ADMIN)
          .subscribe((employes) => {
            this.employes = employes;
            this.employes.forEach((employe) => {
              var averageWorkingHours = 0;
              this.avgworks.forEach((avg) => {
                if (avg._id == employe._id) {
                  averageWorkingHours += avg.averageWorkingHours || 0;
                }
              });
              var avgpush: Avgwork = {
                _id: employe._id,
                firstName: employe.firstName,
                lastName: employe.lastName,
                averageWorkingHours: averageWorkingHours,
              };
              this.datatable.push(avgpush);
            });
          });
          this.loading = false;
      });
  }

  decimalToTimeString(decimalNumber: number): string {
    // Get the integer part (hours)
    const hours = Math.floor(decimalNumber);
    
    // Get the decimal part (minutes)
    const minutesDecimal = (decimalNumber - hours) * 60;
    const minutes = Math.round(minutesDecimal);

    // Format the hours and minutes as HH:mm
    const hoursString = String(hours).padStart(2, '0');
    const minutesString = String(minutes).padStart(2, '0');

    return `${hoursString}:${minutesString}`;
}

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }
}
