import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvgworkComponent } from './avgwork.component';

describe('AvgworkComponent', () => {
  let component: AvgworkComponent;
  let fixture: ComponentFixture<AvgworkComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AvgworkComponent]
    });
    fixture = TestBed.createComponent(AvgworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
