import { CrudSpendingModule } from './crud-spending/crud-spending.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminSideRoutingModule } from './admin-side-routing.module';
import { CrudServiceModule } from './crud-service/crud-service.module';
import { CrudEmployeModule } from './crud-employe/crud-employe.module';
import { MessageService } from 'primeng/api';
import { AboutEmployeModule } from './about-employe/about-employe.module';
import { CrudSpendingtypeComponent } from './crud-spendingtype/crud-spendingtype.component';
import { CrudSpendingtypeModule } from './crud-spendingtype/crud-spendingtype.module';
import { CrudSpendingComponent } from './crud-spending/crud-spending.component';
import { ReservationComponent } from './reservation/reservation.component';
import { ReservationModule } from './reservation/reservation.module';
import { EarningModule } from './earning/earning.module';
import { BenefitComponent } from './benefit/benefit.component';
import { BenefitModule } from './benefit/benefit.module';
import { AvgworkComponent } from './avgwork/avgwork.component';
import { AvgworkModule } from './avgwork/avgwork.module';
import ListOffreModule from '../client/list-offre/list-offre.module';
import ListOffreAdminModule from './list-offre-admin/list-offre.module';
import CreateOffreModule from './create-offre/create-offre.module';

@NgModule({
    declarations: [
  
  ],
    imports: [
        CommonModule,
        AdminSideRoutingModule,
        CrudServiceModule,
        CrudEmployeModule,
        AboutEmployeModule,
        CrudSpendingtypeModule,
        CrudSpendingModule,
        ReservationModule,
        EarningModule,
        BenefitModule,
        AvgworkModule,
        ListOffreAdminModule,
        CreateOffreModule
      ],
    providers: [MessageService]
})
export class AdminSideModule { }
