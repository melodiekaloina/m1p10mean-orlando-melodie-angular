import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TimeScale } from "chart.js";
import { MessageService } from "primeng/api";
import { side } from "src/app/helpers/utils";
import { Employe, Workinghour } from "src/app/models/employe.model";
import { Service } from "src/app/models/service.model";
import { AuthenService } from "src/app/services/authen.service";
import { EmployeService } from "src/app/services/employe.service";
import { ServiceService } from "src/app/services/service.service";

@Component({
  selector: "app-about-employe",
  templateUrl: "./about-employe.component.html",
  styleUrls: ["./about-employe.component.css"],
})
export class AboutEmployeComponent {
  tokenAdmin!: string;
  employe!: Employe;
  time: string = "";

  services!: Service[];

  availableServices: Service[] | undefined;

  selectedServices: Service[] | undefined;

  draggedService: Service | undefined | null;

  constructor(
    private employeService: EmployeService,
    private messageService: MessageService,
    private authenService: AuthenService,
    private serviceService: ServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.tokenAdmin = localStorage.getItem("sessionAdmin") || "";
    this.route.params.subscribe((params) => {
      const employeId = params["id"];
      if (employeId) {
        this.employeService
          .getAllEmploye(this.tokenAdmin, side.ADMIN)
          .subscribe((employes) => {
            this.employe = this.employeService.getEmployeById(
              employeId,
              employes
            );
            if (!this.employe.workhour || this.employe.workhour.length ==0) {
              this.employeService.getDefaultWorkingHour().then(list => {
                this.employe.workhour = list;                
              });
            }
            this.employe.workhour?.forEach(work=>{
              work.dates = [
                // @ts-ignore
                [this.parseToDate(work.times[0][0]), this.parseToDate(work.times[0][1])],
                // @ts-ignore
                [this.parseToDate(work.times[1][0]), this.parseToDate(work.times[1][1])],
              ];
            });

            this.serviceService.getAllService(this.tokenAdmin, side.ADMIN).subscribe(services=>{
              this.services = services;
    
              this.availableServices = services;
              this.selectedServices = [];
              this.employe.tasks?.forEach(idService=>{
                this.selectedServices?.push(this.serviceService.getServiceById(idService, this.services));
                this.availableServices = this.availableServices?.filter((val, i) => val._id != idService);
              });
            });
          });
      }
    });
  }

  formatDate(date: string | Date) {
    return "string" == typeof date ? new Date(date) : date;
  }

  parseToDate(string:string):Date{
     const [hoursStr, minutesStr] = string.split(':');

     const hours = parseInt(hoursStr, 10);
     const minutes = parseInt(minutesStr, 10);
 
     const currentDate = new Date();
 
     currentDate.setHours(hours);
     currentDate.setMinutes(minutes);
     currentDate.setSeconds(0);
 
     return currentDate;
 }
 onSelect(value:Date,idwork:number, id0:number, id1:number) {
  let hour = value.getHours();
  let min = value.getMinutes();
  const foundWorkhour = this.employe.workhour?.find(work => work.dayofweek == idwork);  
  if(this.employe.workhour && foundWorkhour){

    if(min < 10) { 
      foundWorkhour.times[id0][id1]=`${hour}:0${min}`;
      
    } else { 
      foundWorkhour.times[id0][id1]=`${hour}:${min}`;     
    } 
    this.employe.workhour[idwork-1] = foundWorkhour;
    foundWorkhour.dates[id0][id1]=value;    
  
  }
  }

submitWork(){
  const data = { workhour : this.employe.workhour}
  this.employeService.updateWorkingHour(this.tokenAdmin, side.ADMIN,this.employe._id||'', data).subscribe(employe => {
    this.messageService.add({
      severity: "success",
      summary: "Successful",
      detail: "Modifier avec succes",
      life: 3000,
    });    
    employe.workhour?.forEach(work=>{
      work.dates = [
        // @ts-ignore
        [this.parseToDate(work.times[0][0]), this.parseToDate(work.times[0][1])],
        // @ts-ignore
        [this.parseToDate(work.times[1][0]), this.parseToDate(work.times[1][1])],
      ];
    });
    this.employe = employe;
  });
}

  sendConfirmationEmail() {
    // send email
    this.authenService.sendEmail(this.employe).subscribe(
      (data) => {
        this.messageService.add({
          severity: "success",
          summary: "Successful",
          detail: "Email Envoyer",
          life: 3000,
        });
      },
      (error) => {
        this.messageService.add({
          severity: "error",
          summary: "Error",
          detail: "Erreur pendant l'envoye de l'email",
          life: 3000,
        });
      }
    );
  }

  dropService() {
    console.log(this.draggedService);
    if (this.draggedService && this.draggedService._id) {
      const idDropped = this.draggedService._id;
      this.employeService.addTask(this.tokenAdmin, side.ADMIN,this.employe._id||'', idDropped).subscribe(data=>{
          this.employe.tasks?.push(idDropped);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Le service a été ajouté dans vos préférences.', life: 3000 });
      
      // }
    });
    this.selectedServices = [...(this.selectedServices as Service[]), this.draggedService];
    this.availableServices = this.availableServices?.filter((val, i) => val._id != this.draggedService?._id);
    this.draggedService = null;
        console.log(this.employe);
    }
  }
    removeService(obj:Service){ 
      if(obj._id){
        this.employeService.removeTask(this.tokenAdmin, side.ADMIN,this.employe._id||'', obj._id).subscribe(data=>{ 
        
        this.employe.tasks = this.employe.tasks?.filter((val,i)=> val != obj._id);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Le service a été retiré de vos préférences.', life: 3000 });
      // add on availableService
      this.availableServices = [...(this.availableServices as Service[]), obj];
      // delete on selectedService
      this.selectedServices = this.selectedServices?.filter((val, i) => val._id != obj._id);
        });
      }
    }
  

  dragEndService() {
    this.draggedService = null;
}

dragStartService(service: Service) {
  this.draggedService = service;
}
  
  
}