import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutEmployeComponent } from './about-employe.component';

describe('AboutEmployeComponent', () => {
  let component: AboutEmployeComponent;
  let fixture: ComponentFixture<AboutEmployeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AboutEmployeComponent]
    });
    fixture = TestBed.createComponent(AboutEmployeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
