import { DatePipe } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { side } from 'src/app/helpers/utils';
import { Admin } from 'src/app/models/admin.model';
import { Offer } from 'src/app/models/appointment.model';
import { Employe } from 'src/app/models/employe.model';
import { Service } from 'src/app/models/service.model';
import { AdminService } from 'src/app/services/admin.service';
import { OfferService } from 'src/app/services/offer.service';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-create-offre',
  templateUrl: './create-offre.component.html',
  styleUrls: ['./create-offre.component.css'],
  
})
export class CreateOffreComponent {
  tokenAdmin: string = "";

  admin?: Admin;
  offer: Offer = {};

  services?: Service[];

  selectedService?: Service;

  serviceItems: any[] = [];

  reduction: number = 0;

  offername?: string;

  constructor(
    private offerService: OfferService,
    private adminService: AdminService,
    private serviceService: ServiceService,
    private datePipe: DatePipe,
    private router: Router,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.offer.services = [];
    this.tokenAdmin = localStorage.getItem("sessionAdmin") || "";
    this.adminService
      .getAdminByToken(this.tokenAdmin)
      .subscribe((admin) => {
        this.admin = admin;
      });
    this.serviceService
      .getAllService(this.tokenAdmin, side.ADMIN)
      .subscribe((services) => {
        this.services = services;
        this.services.forEach((service) => {
          this.serviceItems.push({
            label: service.servicename,
            value: service,
          });
        });
        this.serviceItems = [... this.serviceItems];
        console.log(this.serviceItems);
      });

  }

  onChooseService(service: any) {
    this.reduction = 0;
    this.selectedService = service;
  }


  onDateStartSelect(value: any) { 
   
      this.offer.start = this.formatDateString(value);
   
    
    this.offer.services = [];
    // init value selectedService, selectedEmploye, availableEmployes, availabeEmployeItems
    this.selectedService = undefined;
    this.offer.end = undefined;
  }

  onDateEndSelect(value: any) { 
    if (this.offer.start && (value < new Date(this.offer.start))) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'La date fin ne peut être antérieure à la date debut.', life: 3000 });     
      this.offer.end = undefined; 
    }else{
      this.offer.end = this.formatDateString(value);
    }
    
    this.offer.services = [];
    // init value selectedService, selectedEmploye, availableEmployes, availabeEmployeItems
    this.selectedService = undefined;
  }


  addServiceOffer() {
    // if efa misy ao @ list le couple emp&service dia tsy ampina
    const program = {
      service: this.selectedService?._id,
      serviceobj: this.selectedService,
      reduction: this.reduction
    };
    if (!this.alreadyInList(program)) {
      this.offer.services?.push(program);
    }
    //init value selectedService, selectedEmploye, availableEmployes, availableEmployeItems
    this.selectedService = undefined;
  }

  removeServiceOffer(service: any) {
    let removeIndex = this.findIndex(service);
    this.offer.services = this.offer.services?.filter(
      (val, i) => i != removeIndex
    );
    console.log(this.offer.services);
  }
  findIndex(service: any) {
    let index = -1;
    for (let i = 0; i < this.offer.services!.length; i++) {
      if (
        service._id == this.offer.services![i].service
      ) {
        index = i;
        break;
      }
    }
    return index;
  }
  alreadyInList(service: any): boolean {
    for (let i = 0; i < this.offer.services!.length; i++) {
      if (
        service._id == this.offer.services![i].service
      ) {
        return true;
      }
    }
    return false;
  }
  create() {
    if(!this.offername){
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Le nom de l\'offre ne peut pas etre vide.', life: 3000 });
      return;
    }
    if(!this.offer.start){
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Veuillez sélectionner la date et l\'heure de de l\'offre.', life: 3000 });
      return;
    }
    if(!this.offer.services||this.offer.services?.length==0){
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Il est obligatoire d\'ajouter un ou plusieurs programmes.', life: 3000 });
      return ;      
  }  
  this.offer.offername = this.offername;
    // create offer
    // console.log(this.offer);
      this.offerService.addOffer(this.tokenAdmin, side.ADMIN,this.offer).subscribe(data=>{
        //redirect 
        this.router.navigate([`/admin/list-offre`]);
      },
        error=>{
          if(error.status == 500){
            this.messageService.add({ severity: 'error', summary: 'Error', detail: error.error.data.message, life: 3000 });
          }
        } 
      )
  }
  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-ddTHH:mm")!;
  }
  formatDate(date: string) {
    return new Date(date);
  }
  todayDateStr() {
    let today = new Date();

    let year = today.getFullYear().toString();
    let month = (today.getMonth() + 1).toString().padStart(2, "0");
    let day = today.getDate().toString().padStart(2, "0");

    let dateString = `${year}-${month}-${day}`;
    return dateString;
  }

} 
