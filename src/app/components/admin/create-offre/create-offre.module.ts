import { ChipModule } from 'primeng/chip';
import { DialogModule } from 'primeng/dialog';
import { ToolbarModule } from 'primeng/toolbar';
import { NgModule } from "@angular/core";
import { CarouselModule } from 'primeng/carousel';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ButtonModule } from "primeng/button";
import { DropdownModule } from "primeng/dropdown";
import { InputTextModule } from "primeng/inputtext";
import { ProgressBarModule } from "primeng/progressbar";
import { RatingModule } from "primeng/rating";
import { RippleModule } from "primeng/ripple";
import { TableModule } from "primeng/table";
import { ToastModule } from "primeng/toast"; 
import { ToggleButtonModule } from "primeng/togglebutton";
import { MultiSelectModule } from 'primeng/multiselect';
import { CalendarModule } from "primeng/calendar";
import { CreateOffreComponent } from "./create-offre.component";
import { SplitButtonModule } from "primeng/splitbutton";
import { ListboxModule } from 'primeng/listbox';
import { InputNumberModule } from 'primeng/inputnumber';

@NgModule({
    declarations: [CreateOffreComponent],
    imports: [
        CommonModule,
        ButtonModule,
        RippleModule,
        SplitButtonModule,
        ToggleButtonModule,
        ToolbarModule,
        ToastModule,        
		DialogModule,
        CalendarModule,
        FormsModule,
        ChipModule,
        DropdownModule,
        ReactiveFormsModule,
        ListboxModule,
        InputNumberModule
    ],
    exports: [CreateOffreComponent]
})

export default class CreateOffreModule { }