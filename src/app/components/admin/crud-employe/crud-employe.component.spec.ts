import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudEmployeComponent } from './crud-employe.component';

describe('CrudEmployeComponent', () => {
  let component: CrudEmployeComponent;
  let fixture: ComponentFixture<CrudEmployeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CrudEmployeComponent]
    });
    fixture = TestBed.createComponent(CrudEmployeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
