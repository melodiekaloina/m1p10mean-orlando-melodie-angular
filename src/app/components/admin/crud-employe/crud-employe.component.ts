import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { side } from 'src/app/helpers/utils';
import { Employe } from 'src/app/models/employe.model';
import { EmployeService } from 'src/app/services/employe.service';

@Component({
  selector: 'app-crud-employe',
  templateUrl: './crud-employe.component.html',
  styleUrls: ['./crud-employe.component.css']
}) 
export class CrudEmployeComponent implements OnInit {
  tokenAdmin!:string;
  employes!: Employe[];
  employe!: Employe;
  loading:boolean = true;

  employeDialog: boolean = false;

  deleteEmployeDialog: boolean = false;

  submitted: boolean = false;

  cols: any[] = [];

  globalFilterFields: any[] = [];

  rowsPerPageOptions = [10, 20, 30];

  emailError: string | undefined;

  errorField!: any[]; 



  constructor(private employeService: EmployeService, private messageService: MessageService, private router: Router, private fb: FormBuilder, private datePipe: DatePipe) { }

  // required: cin, firstname, lastname, address, datebirth
  ngOnInit() {
    this.tokenAdmin = localStorage.getItem('sessionAdmin')||'';
    this.errorField = [
      { field: "firstName", header: "Nom" },
      { field: "lastName", header: "Prenom" },
      { field: "cin", header: "CIN" },
      { field: 'address', header: "Address" },
      { field: 'datebirth', header: "Date de naissance" },
    ];
 
    
        this.cols = [
          { field: 'firstName', header: 'Nom' },
          { field: 'lastName', header: 'Prenom' },
          { field: 'email', header: 'Email' },
          { field: 'hired_at', header: 'Embaucher le' },
        ];
    
        this.globalFilterFields = ['firstName', 'lastName', 'email', 'hired_at'];
    this.employeService.getAllEmploye(this.tokenAdmin, side.ADMIN).subscribe(employes => {
      this.employes = employes;
      this.loading = false;
    })
  }


  hideDialog() {
    this.initValue();
    this.employeDialog = false;
    this.submitted = false;
  }

  openNew() {
    this.employe = {};
    this.submitted = false;
    this.employeDialog = true;
  }

  editEmploye(employe: Employe) {
    this.employe = { ...employe };
    this.employeDialog = true;
    this.employe.datebirth = new Date(this.employe.datebirth || '');
    if(this.employe.hired_at){
      this.employe.hired_at = new Date(this.employe.hired_at);
    }
  }

  deleteEmploye(employe: Employe) {
    this.deleteEmployeDialog = true;
    this.employe = { ...employe };
  }

  confirmDelete() {
    this.deleteEmployeDialog = false;
    this.employeService.deleteEmploye(this.tokenAdmin, side.ADMIN,this.employe._id || '').subscribe(employeItem => {
      this.employes = this.employes.filter(val => val._id !== this.employe._id);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Employe Deleted', life: 3000 });
      this.employe = {};
    });
  }

  saveEmploye() {
    this.submitted = true;
    this.initValue();
    //control
    if (!this.employe.email || (this.employe.email && this.employe.email.trim() == '')) {
      this.emailError = 'Email est requis.';
    }
    if(this.employe.email && !this.isValidEmail(this.employe.email)){
      this.emailError = "Format d'email non valide";
    }

    var requiredInvalid = false;
    this.errorField.forEach(field => {
      const fieldValue = (this.employe as any)[field.field];
      if (fieldValue == undefined || fieldValue == '') {
        this.setErrorForField(field.field, field.header + ' est requis.');
        requiredInvalid = true;
      }
    });
    if (requiredInvalid || this.emailError) {
      return;
    }

    

    if (this.employe.firstName?.trim() && this.employe.email?.trim()) {
      if (this.employe.datebirth) {
        this.employe.datebirth = this.datePipe.transform(new Date(this.employe.datebirth), 'yyyy-MM-dd') || '';
      }
      if (this.employe.hired_at) {
        this.employe.hired_at = this.datePipe.transform(new Date(this.employe.hired_at), 'yyyy-MM-dd') || undefined;
      } else { this.employe.hired_at = undefined;}

      // update
      if (this.employe._id) {
        // @ts-ignore
        this.employeService.updateEmploye(this.tokenAdmin, side.ADMIN, this.employe._id, this.employe).subscribe(
          employeItem => {          
          this.employes[this.findIndexById(employeItem._id || '')] = employeItem;
          this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Employe Updated', life: 3000 });
        }
        , error => {
            if (error.status === 500) {
              this.messageService.add({ severity: 'error', summary: 'Error', detail: 'L\'email existe deja', life: 3000 });
            }
          }
          );



      }
      // create 
      else {
        // this.employe._id = this.createId();
        // @ts-ignore        
        this.employe.password = "";
        this.employeService.addEmploye(this.tokenAdmin, side.ADMIN,this.employe).subscribe(
          employeItem => {
            this.employes.push(employeItem);
            this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Employe Created', life: 3000 });
          }
          , error => {
            console.log(error);
            if (error.status === 401) {
              this.messageService.add({ severity: 'error', summary: 'Error', detail: 'L\email existe deja', life: 3000 });
            }
          }
        );

      }

      this.employes = [...this.employes];
      this.employeDialog = false;
      this.employe = {};
    }

  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.employes.length; i++) {
      if (this.employes[i]._id === id) {
        index = i;
        break;
      }
    }

    return index;
  }

  createId(): string {
    let id = '';
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, 'contains');
  }

  initValue() {
    this.emailError = undefined;
    //init errorField
    this.errorField.forEach(field => {
      this.setErrorForField(field.field, undefined);
    });
  }

  getErrorByFieldName(fieldName: string) {
    return this.errorField.find(field => field.field === fieldName);
  }

  setErrorForField(fieldName: string, errorMessage: string | undefined): void {
    const fieldObj = this.errorField.find(field => field.field === fieldName);
    if (fieldObj) {
      fieldObj.error = errorMessage;
    }
  }

  formatDate(date: string | Date) {
    return ("string" == typeof date) ? new Date(date) : date;
  }

  isValidEmail(email: string): boolean {
    // Regular expression to validate email format
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }


  redirectToAbout(idEmploye: string) {
    this.router.navigate([`/admin/about-employe/${idEmploye}`]);
  }


} 
