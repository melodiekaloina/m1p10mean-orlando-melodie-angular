import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudSpendingtypeComponent } from './crud-spendingtype.component';

describe('CrudSpendingtypeComponent', () => {
  let component: CrudSpendingtypeComponent;
  let fixture: ComponentFixture<CrudSpendingtypeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CrudSpendingtypeComponent]
    });
    fixture = TestBed.createComponent(CrudSpendingtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
