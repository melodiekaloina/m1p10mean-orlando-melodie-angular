import { SpendingService } from "./../../../services/spending.service";
import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { Table } from "primeng/table";
import { side } from "src/app/helpers/utils";
import { Spendingtype } from "src/app/models/admin.model";
import { AdminService } from "src/app/services/admin.service";

@Component({
  selector: "app-crud-spendingtype",
  templateUrl: "./crud-spendingtype.component.html",
  styleUrls: ["./crud-spendingtype.component.css"],
})
export class CrudSpendingtypeComponent {
  tokenAdmin!: string;
  spendingtypes!: Spendingtype[];
  spendingtype!: Spendingtype;
  loading:boolean = true;

  spendingtypeDialog: boolean = false;

  deleteSpendingtypeDialog: boolean = false;

  submitted: boolean = false;

  cols: any[] = [];

  globalFilterFields: any[] = [];

  rowsPerPageOptions = [10, 20, 30];

  errorField!: any[];

  constructor(
    private adminService: AdminService,
    private messageService: MessageService,
    private router: Router,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private spendingService: SpendingService
  ) {}

  // required: cin, firstname, lastname, address, datebirth
  ngOnInit() {
    this.tokenAdmin = localStorage.getItem("sessionAdmin") || "";
    this.errorField = [
      { field: "spendinglabel", header: "Nom" },
      { field: "code", header: "CODE" },
    ];

    
        this.cols = [
          { field: "spendinglabel", header: "Nom" },
          { field: "spendingvalue", header: "Valeur" },
          { field: "code", header: "CODE" },
        ];
        this.globalFilterFields = ["spendinglabel", "spendingvalue", "code"];

    this.spendingService
      .getAllspendingtype(this.tokenAdmin, side.ADMIN)
      .subscribe((spendingtypes) => {
        this.spendingtypes = spendingtypes;
        this.loading = false;
      });        
  }

  hideDialog() {
    this.initValue();
    this.spendingtypeDialog = false;
    this.submitted = false;
  }

  openNew() {
    this.spendingtype = {};
    this.submitted = false;
    this.spendingtypeDialog = true;
  }

  editSpendingtype(spendingtype: Spendingtype) {
    this.spendingtype = { ...spendingtype };
    this.spendingtypeDialog = true;
  }

  deleteSpendingtype(spendingtype: Spendingtype) {
    this.deleteSpendingtypeDialog = true;
    this.spendingtype = { ...spendingtype };
  }

  confirmDelete() {
    this.deleteSpendingtypeDialog = false;
    // check if deletable
    this.spendingService
      .deleteSpendingtype(
        this.tokenAdmin,
        side.ADMIN,
        this.spendingtype._id || ""
      )
      .subscribe(
        (spendingtypeItem) => {
          this.spendingtypes = this.spendingtypes.filter(
            (val) => val._id !== this.spendingtype._id
          );
          this.messageService.add({
            severity: "success",
            summary: "Successful",
            detail: "Spendingtype Deleted",
            life: 3000,
          });
          this.spendingtype = {};
        },
        (error) => {
          this.messageService.add({ severity: 'error', summary: 'Error', detail: error.error.data.message, life: 3000 });
        }
      );
  }

  saveSpendingtype() {
    this.submitted = true;
    this.initValue();
    //control

    var requiredInvalid = false;
    this.errorField.forEach((field) => {
      const fieldValue = (this.spendingtype as any)[field.field];
      if (fieldValue == undefined || fieldValue == "") {
        this.setErrorForField(field.field, field.header + " est requis.");
        requiredInvalid = true;
      }
    });
    if (requiredInvalid) {
      return;
    }

    this.spendingtype.spendingvalue = !this.spendingtype.spendingvalue
      ? 0
      : this.spendingtype.spendingvalue;
    this.spendingtype.code = this.spendingtype.code?.toUpperCase();

    // update
    if (this.spendingtype._id) {
      this.spendingService
        .updateSpendingtype(
          this.tokenAdmin,
          side.ADMIN,
          this.spendingtype._id,
          this.spendingtype
        )
        .subscribe(
          (spendingtypeItem) => {
            console.log(spendingtypeItem);
            this.spendingtypes[this.findIndexById(spendingtypeItem._id || "")] =
              spendingtypeItem;
            this.messageService.add({
              severity: "success",
              summary: "Successful",
              detail: "Spendingtype Updated",
              life: 3000,
            });
          },
          (error) => {
            // if (error.status === 500) {
            //   this.messageService.add({ severity: 'error', summary: 'Error', detail: error.message, life: 3000 });
            // }
          }
        );
    }
    // create
    else {
      this.spendingService
        .addSpendingtype(this.tokenAdmin, side.ADMIN, this.spendingtype)
        .subscribe(
          (spendingtypeItem) => {
            this.spendingtypes.push(spendingtypeItem);
            this.messageService.add({
              severity: "success",
              summary: "Successful",
              detail: "Spendingtype Created",
              life: 3000,
            });
          },
          (error) => {
            // if (error.status === 401) {
            //   this.messageService.add({ severity: 'error', summary: 'Error', detail: error.message, life: 3000 });
            // }
          }
        );
    }

    this.spendingtypes = [...this.spendingtypes];
    this.spendingtypeDialog = false;
    this.spendingtype = {};
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.spendingtypes.length; i++) {
      if (this.spendingtypes[i]._id === id) {
        index = i;
        break;
      }
    }

    return index;
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }

  initValue() {
    //init errorField
    this.errorField.forEach((field) => {
      this.setErrorForField(field.field, undefined);
    });
  }

  getErrorByFieldName(fieldName: string) {
    return this.errorField.find((field) => field.field === fieldName);
  }

  setErrorForField(fieldName: string, errorMessage: string | undefined): void {
    const fieldObj = this.errorField.find((field) => field.field === fieldName);
    if (fieldObj) {
      fieldObj.error = errorMessage;
    }
  }
}
