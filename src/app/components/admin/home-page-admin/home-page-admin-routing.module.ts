import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomePageAdminComponent } from './home-page-admin.component';

@NgModule({
    imports: [RouterModule.forChild([
        { path: '', component: HomePageAdminComponent }
    ])],
    exports: [RouterModule]
})
export class HomePageAdminRoutingModule { }
