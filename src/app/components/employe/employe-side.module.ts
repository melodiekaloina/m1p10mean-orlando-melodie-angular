import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeSideRoutingModule } from './employe-side-routing.module';
import ListRdvEmployeModule from './list-rdv-employe/list-rdv-employe.module';
import { AboutAppointEmployeModule } from './about-appoint-employe/about-appoint-employe.module';
import { ListProgramComponent } from './list-program/list-program.component';
import ListProgramModule from './list-program/list-program.module';
import { AboutProgramComponent } from './about-program/about-program.component';
import AboutProgramModule from './about-program/about-program.module';
import ListOffreModule from '../client/list-offre/list-offre.module';
import AboutOffreModule from '../client/about-offre/about-offre.module';
import { HoraireModule } from './horaire/horaire.module';

@NgModule({
    declarations: [
  
  ],
    imports: [
        CommonModule,
        EmployeSideRoutingModule,
        ListRdvEmployeModule,
        AboutAppointEmployeModule,
        ListProgramModule,
        AboutProgramModule,
        HoraireModule
        
    ]
})
export class EmployeSideModule { }
