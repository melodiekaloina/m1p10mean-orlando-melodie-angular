import { DatePipe } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Table } from 'primeng/table';
import { appointmentState, side } from 'src/app/helpers/utils';
import { Appointment, AppointmentItem } from 'src/app/models/appointment.model';
import { AppointClient } from 'src/app/models/client.model';
import { Employe } from 'src/app/models/employe.model';
import { Service } from 'src/app/models/service.model';
import { AppointmentService } from 'src/app/services/appointment.service';
import { EmployeService } from 'src/app/services/employe.service';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-list-program',
  templateUrl: './list-program.component.html',
  styleUrls: ['./list-program.component.css']
})
export class ListProgramComponent {
  tokenEmploye!: string;
  loading: boolean = true;

  employe?: Employe;

  dateFilter!: string;

  commission!: number;

  countcommission!: number;

  programs!: AppointmentItem[];

  employes!: Employe[];

  services!: Service[];

  rdvstatuses: any[] = [];

  appointState = appointmentState;

  @ViewChild("filter") filter!: ElementRef;

  constructor(
    private appointmentService: AppointmentService,
    private employeService: EmployeService,
    private serviceService: ServiceService,
    private datePipe: DatePipe,
    private router: Router
  ) {}

  ngOnInit() {
    this.dateFilter = this.todayDateStr();
    this.tokenEmploye = localStorage.getItem("sessionEmp") || "";
    this.employeService
      .getEmployeByToken(this.tokenEmploye)
      .subscribe((employe) => {
        this.employe = employe;
        this.initData();
        this.loading = false;
        this.serviceService.getAllService(this.tokenEmploye, side.EMP).subscribe(services=>this.services = services);
      });

      this.rdvstatuses = [
        { label: "Payé", value: this.appointState.PAID },
        { label: "Fait", value: this.appointState.DONE },
        { label: "Annulé", value: this.appointState.CANCELED },
        { label: "En cours", value: this.appointState.ONGOING },
        { label: "Créer", value: this.appointState.CREATED }
      ];
  }

  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = "";
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }

  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-dd")!;
  }
  getRdvStatusByCode(value: number|undefined) {
    return this.rdvstatuses.find(status => status.value == value);
  }
  redirectToAbout(idAppointment: string) {
    this.router.navigate([`/employe/about-program/${idAppointment}`]);
  }
  setDateFilter(value: string) {
    this.dateFilter = value;
    //
    this.initData();
  }
  initData(){
    this.employeService.commission(this.tokenEmploye, side.EMP, this.employe?._id||'', this.dateFilter).subscribe(detail=>{
      if(detail.data.commission){
        this.commission = detail.data.commission.sum; 
        this.countcommission = detail.data.commission.count; 
      }else{
        this.commission = 0; 
        this.countcommission = 0; 
      }
      if(detail.data.today){
        this.programs = [];
        (detail.data.today as any[]).forEach(element => {
          this.programs.push(
            {
              employee: element.employee,
              service: element.service._id,
              serviceobj: element.service,
              start: element.start,
              end: element.end,
              price: element.price,
              state: element.state,
              _id: element._id
            } as AppointmentItem
            );
        });
        this.programs = [... this.programs]
      }
    });
  }
  todayDateStr() {
    let today = new Date();

    let year = today.getFullYear().toString();
    let month = (today.getMonth() + 1).toString().padStart(2, "0");
    let day = today.getDate().toString().padStart(2, "0");

    let dateString = `${year}-${month}-${day}`;
    return dateString;
  }
  calculateHours(minuteValue:number|undefined): number {
    return (minuteValue)?Math.floor(minuteValue / 60):-1;
  } 

  calculateRemainingMinutes(minuteValue: number|undefined): number {
    return (minuteValue)?minuteValue % 60:-1;
  }

}
