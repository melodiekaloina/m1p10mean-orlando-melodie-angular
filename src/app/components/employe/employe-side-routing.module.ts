import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ListRdvEmployeComponent } from './list-rdv-employe/list-rdv-employe.component';
import { AboutAppointEmployeComponent } from './about-appoint-employe/about-appoint-employe.component';
import { ListProgramComponent } from './list-program/list-program.component';
import { AboutProgramComponent } from './about-program/about-program.component';
import { HoraireComponent } from './horaire/horaire.component';

@NgModule({
    imports: [RouterModule.forChild([
        // { path: '', loadChildren: () => import('./home-page-employe/home-page-employe.module').then(m => m.HomePageEmployeModule) },
        { path: '', component: ListProgramComponent }, 
        { path: 'about-appoint/:id', component: AboutAppointEmployeComponent },
        { path: 'about-program/:id', component: AboutProgramComponent },
        { path: 'horaire', component: HoraireComponent },
        
        // { path: '**', redirectTo: '/notfound' }
    ])], 
    exports: [RouterModule]
})
export class EmployeSideRoutingModule { }
