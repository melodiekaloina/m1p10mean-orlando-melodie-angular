import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Password } from 'primeng/password';
import { stringToSha1 } from 'src/app/helpers/utils';
import { LayoutService } from 'src/app/layouts/bolayouts/service/app.layout.service';
import { Employe } from 'src/app/models/employe.model';
import { EmployeService } from 'src/app/services/employe.service';

@Component({
  selector: 'app-edit-password',
  templateUrl: './edit-password.component.html',
  styleUrls: ['./edit-password.component.scss']
})
export class EditPasswordComponent {
  // if id employe not recognise then redirect not found
  // check if localstorage contain the confirm code md5 
  // else redirect -> code-password 

  employe!: Employe;

  idEmploye!: string;

  passwordInput!: string;
  password!: string;
  passwordError!: string | undefined;

  confirmPasswordInput!: string;
  confirmPassword!: string;
  confirmPasswordError: string | undefined;

  submitted: boolean = false;

  constructor(public layoutService: LayoutService, public messageService: MessageService, private router: Router, private route: ActivatedRoute, private employeService: EmployeService,) {

  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const employeId = params['id'];
      if (employeId) {
        this.employeService.getAllEmployeFree().subscribe(employes => {
          try {

            this.employe = this.employeService.getEmployeById(employeId, employes);
          } catch (error) {
            // redirect 
            this.router.navigate([`/notfound`]);
            return;
          }
          this.idEmploye = employeId;

            if (!window.history.state.allow ) {
              this.router.navigate([`/employe/code-pswrd/${employeId}`]);
              return;
            }
        })
      }
    });
  }
  submit() {
    this.initValue();
    this.submitted = true;
    // control
    // check password and confirmPassword
    if (!this.password) {
      this.passwordError = "Mot de passe requis.";
      return;
    }
    if (!this.confirmPassword) {
      this.confirmPasswordError = "Mot de passe de confirmation requis.";
      return;
    }
    if (this.password != this.confirmPassword) {
      this.confirmPasswordError = "Le mot de passe saisi ne correspond pas au mot de passe de confirmation.";
      return;
    }

    this.employeService.updatePasswordEmploye(this.idEmploye,this.passwordInput).subscribe(data =>{
      window.history.replaceState({allow:false},'');
      this.router.navigate(['/employe']);
    });

  }

  updatePassword(value: string): void {    
    this.password = stringToSha1(value);    
    if (this.submitted) {
      this.submitted = false;
      this.initValue();
    }

  }

  updateConfirmPassword(value: any): void {    
    this.confirmPassword = stringToSha1(value);
    if (this.submitted) {
      this.submitted = false;
      this.initValue();
    }

  }

  initValue() {
    this.confirmPasswordError = undefined;
    this.passwordError = undefined;

  }
}
