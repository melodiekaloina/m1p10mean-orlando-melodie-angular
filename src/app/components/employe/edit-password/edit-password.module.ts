import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule } from '@angular/forms';
import { PasswordModule } from 'primeng/password';
import { InputTextModule } from 'primeng/inputtext';
import { RouterModule } from '@angular/router';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { EditPasswordComponent } from './edit-password.component';
import { EmployeService } from 'src/app/services/employe.service';

@NgModule({
    imports: [
        CommonModule,
        ButtonModule,
        CheckboxModule,
        InputTextModule,
        FormsModule,
        PasswordModule, 
        RouterModule,
        ToastModule,
        
    ],
    declarations: [EditPasswordComponent],
    providers: [ MessageService ]
})
export class EditPasswordModule { }
