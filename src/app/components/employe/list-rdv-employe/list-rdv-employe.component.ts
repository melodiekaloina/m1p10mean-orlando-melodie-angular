import { AppointmentService } from "../../../services/appointment.service";

import {
  Component,
  ElementRef,
  ViewChild,
  ChangeDetectorRef,
  OnChanges,
  SimpleChanges,
} from "@angular/core";
import { Customer, Representative } from "../../bocomponents/api/customer";
import { Table } from "primeng/table";
import { Appointment } from "src/app/models/appointment.model";
import { Service } from "src/app/models/service.model";
import { ServiceService } from "src/app/services/service.service";
import { MessageService, ConfirmationService } from "primeng/api";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { ClientService } from "src/app/services/client.service";
import { Client } from "src/app/models/client.model";
import { EmployeService } from "src/app/services/employe.service";
import { appointmentState, side } from "src/app/helpers/utils";

@Component({
  selector: "app-list-rdv-employe",
  templateUrl: "./list-rdv-employe.component.html",
  styleUrls: ["./list-rdv-employe.component.css"],
  providers: [MessageService, ConfirmationService, DatePipe],
})
export class ListRdvEmployeComponent {
  tokenEmploye!: string;
  loading: boolean = true;

  dateFilter!: string;

  commission!: number;

  appointments!: Appointment[];

  clients!: Client[];

  services!: Service[];

  rdvstatuses: any[] = [];

  @ViewChild("dt1") dt1!: Table;
  @ViewChild("filter") filter!: ElementRef;

  constructor(
    private appointmentService: AppointmentService,
    private clientService: ClientService,
    private employeService: EmployeService,
    private serviceService: ServiceService,
    private datePipe: DatePipe,
    private router: Router,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.tokenEmploye = localStorage.getItem("sessionEmp") || "";
    this.employeService
      .getEmployeByToken(this.tokenEmploye)
      .subscribe((employe) => {
        this.clientService.getAllClient().then((clients) => {
          this.clients = clients;

          this.serviceService.getAllService(this.tokenEmploye,side.EMP).subscribe((services) => {
            this.services = services;

            //get Appointment by idClient
            this.appointmentService
              .getAppointmentsByIdEmploye(employe._id||'')
              .then((data) => {
                this.appointments = data;

                this.appointments.forEach((appointment) => {
                  appointment.clientobj = this.clientService.getClientById(
                    appointment.client || "",
                    this.clients
                  );
                  appointment.serviceobj = this.serviceService.getServiceById(
                    appointment.service || "",
                    this.services
                  );
                });
              });
          });
        });
      });
    this.rdvstatuses = [
      { label: "Payé", value: 10 },
      { label: "Pas encore payé", value: 7 },
      { label: "En cours", value: 3 },
      { label: "Dans le programme", value: 0 },
    ];

    this.dateFilter = this.todayDateStr();
  }

  ngAfterViewInit() {
    this.dt1.filter(this.dateFilter, "date", "contains");
    this.loading = false;
    this.cd.detectChanges();
  }

  clear(table: Table) {
    this.loading = true;
    table.clear();  
    this.dt1.filter(this.dateFilter, "date", "contains");
    this.filter.nativeElement.value = "";
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }

  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-dd")!;
  }
  getRdvStatusByCode(value: number) {
    return this.rdvstatuses.find((status) => status.value == value);
  }
  redirectToAboutAppointEmploye(idAppointment: string) {
    this.router.navigate([`/employe/about-appoint/${idAppointment}`]);
  }
  setDateFilter(value: string) {
    this.dateFilter = value;
  }
  todayDateStr() {
    let today = new Date();

    let year = today.getFullYear().toString();
    let month = (today.getMonth() + 1).toString().padStart(2, "0");
    let day = today.getDate().toString().padStart(2, "0");

    let dateString = `${year}-${month}-${day}`;
    return dateString;
  }

  onFilter(event: any, dt: any) {
    const list: Appointment[] = event.filteredValue;
    var commissionValue = 0;
    list.forEach((element) => {
      if (element.state == appointmentState.DONE) {
        const price = element.price ? element.price : 0;
        const commissionPercentage = element.commission
          ? element.commission
          : 0;
        commissionValue += price * (commissionPercentage / 100);
      }
    });
    this.commission = commissionValue;
    this.cd.detectChanges();
  }
}
