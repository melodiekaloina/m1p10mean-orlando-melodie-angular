import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListRdvEmployeComponent } from './list-rdv-employe.component';


describe('ListRdvEmployeComponent', () => {
  let component: ListRdvEmployeComponent;
  let fixture: ComponentFixture<ListRdvEmployeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListRdvEmployeComponent]
    });
    fixture = TestBed.createComponent(ListRdvEmployeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
