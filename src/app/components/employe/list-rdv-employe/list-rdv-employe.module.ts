import { NgModule } from "@angular/core";
import { CarouselModule } from 'primeng/carousel';
import { ListRdvEmployeComponent } from "./list-rdv-employe.component";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ButtonModule } from "primeng/button";
import { DropdownModule } from "primeng/dropdown";
import { InputTextModule } from "primeng/inputtext";
import { ProgressBarModule } from "primeng/progressbar";
import { RatingModule } from "primeng/rating";
import { RippleModule } from "primeng/ripple";
import { TableModule } from "primeng/table";
import { ToastModule } from "primeng/toast";
import { ToggleButtonModule } from "primeng/togglebutton";
import { MultiSelectModule } from 'primeng/multiselect';
import { CalendarModule } from "primeng/calendar";

@NgModule({
    declarations: [ListRdvEmployeComponent],
    imports: [
        CarouselModule,
        CommonModule,
		FormsModule,
		TableModule,
		RatingModule,
		ButtonModule,
		InputTextModule,
		ToggleButtonModule,
		RippleModule,     
		MultiSelectModule,
		DropdownModule,
		ProgressBarModule,
		ToastModule,
		CalendarModule
    ],
    exports: [ListRdvEmployeComponent]
})

export default class ListRdvEmployeModule { }