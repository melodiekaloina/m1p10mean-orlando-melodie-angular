import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageEmployeRoutingModule } from './home-page-employe-routing.module';
import { HomePageEmployeComponent } from './home-page-employe.component';

@NgModule({
    imports: [
        CommonModule,
        HomePageEmployeRoutingModule
    ],
    declarations: [HomePageEmployeComponent]
})
export class HomePageEmployeModule { }
