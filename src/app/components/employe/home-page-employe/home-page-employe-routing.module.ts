import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomePageEmployeComponent } from './home-page-employe.component';

@NgModule({
    imports: [RouterModule.forChild([
        { path: '', component: HomePageEmployeComponent }
    ])],
    exports: [RouterModule]
})
export class HomePageEmployeRoutingModule { }
