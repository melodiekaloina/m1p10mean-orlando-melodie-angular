import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutAppointEmployeComponent } from './about-appoint-employe.component';

describe('AboutAppointEmployeComponent', () => {
  let component: AboutAppointEmployeComponent;
  let fixture: ComponentFixture<AboutAppointEmployeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AboutAppointEmployeComponent]
    });
    fixture = TestBed.createComponent(AboutAppointEmployeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
