import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { appointmentState, side } from "src/app/helpers/utils";
import { Appointment } from "src/app/models/appointment.model";
import { AppointmentService } from "src/app/services/appointment.service";
import { AuthenService } from "src/app/services/authen.service";
import { ClientService } from "src/app/services/client.service";
import { ServiceService } from "src/app/services/service.service";

@Component({
  selector: "app-about-appoint-employe",
  templateUrl: "./about-appoint-employe.component.html",
  styleUrls: ["./about-appoint-employe.component.css"],
})
export class AboutAppointEmployeComponent {
  tokenEmploye!: string;
  appointment!: Appointment;

  rdvstatuses: any[] = [];

  appointState = appointmentState;

  constructor(
    private appointmentService: AppointmentService,
    private clientService: ClientService,
    private serviceService: ServiceService,
    private messageService: MessageService,
    private authenService: AuthenService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.tokenEmploye = localStorage.getItem("sessionEmp") || "";
    this.route.params.subscribe((params) => {
      const appointmentId = params["id"];
      console.log("id ", appointmentId);
      if (appointmentId) {
        this.clientService.getAllClient().then((clients) => {
          this.serviceService.getAllService(this.tokenEmploye,side.EMP).subscribe((services) => {

            this.appointmentService.getAllAppointment().then((appointments) => {
              this.appointment = this.appointmentService.getAppointmentById(
                appointmentId,
                appointments
              );

              this.appointment.clientobj = this.clientService.getClientById(
                this.appointment.client || "",
                clients
              );
              this.appointment.serviceobj = this.serviceService.getServiceById(
                this.appointment.service || "",
                services
              );
            });
          });
        });
       
      }
    });

    this.rdvstatuses = [
      { label: "Payé", value: this.appointState.PAID },
      { label: "Payé", value: this.appointState.DONE },
      { label: "Payé", value: this.appointState.CANCELED },
      { label: "Pas encore payé", value: this.appointState.ONGOING },
      { label: "En cours", value: this.appointState.ONGOING },
      { label: "Dans le programme", value: this.appointState.CREATED },
    ];
  }

  formatDate(date: string | Date) {
    return "string" == typeof date ? new Date(date) : date;
  }

  getRdvStatusByCode(value: number|undefined) {
    return this.rdvstatuses.find(status => status.value == value);
  }
  
  calculateHours(minuteValue:number|undefined): number {
    return (minuteValue)?Math.floor(minuteValue / 60):-1;
  } 

  calculateRemainingMinutes(minuteValue: number|undefined): number {
    return (minuteValue)?minuteValue % 60:-1;
  }

}
