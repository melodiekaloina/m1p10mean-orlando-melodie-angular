import { DatePipe } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { appointmentState, side } from 'src/app/helpers/utils';
import { AppointmentItem } from 'src/app/models/appointment.model';
import { Employe } from 'src/app/models/employe.model';
import { Service } from 'src/app/models/service.model';
import { AppointmentService } from 'src/app/services/appointment.service';
import { AuthenService } from 'src/app/services/authen.service';
import { EmployeService } from 'src/app/services/employe.service';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-about-program',
  templateUrl: './about-program.component.html',
  styleUrls: ['./about-program.component.css']
})
export class AboutProgramComponent {
  tokenEmploye!: string;
  program!: AppointmentItem;

  employe!: Employe;

  rdvstatuses: any[] = [];

  display: boolean = false;

  appointState = appointmentState;


  services!: Service[];

  @ViewChild("filter") filter!: ElementRef;

  loading: boolean = true;

  constructor(
    private appointmentService: AppointmentService,
    private employeService: EmployeService,
    private serviceService: ServiceService,
    private messageService: MessageService,
    private authenService: AuthenService,
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.tokenEmploye = localStorage.getItem("sessionEmp") || "";
    this.route.params.subscribe((params) => {
      const appointmentId = params["id"];

      if (appointmentId) {
        this.employeService
          .getEmployeByToken(this.tokenEmploye)
          .subscribe((employe) => {
            this.employe = employe;
            this.employeService
              .getDetailEmploye(this.tokenEmploye, side.EMP, employe._id || "")
              .subscribe((detail) => {
                    this.serviceService
                      .getAllServiceFree()
                      .subscribe((services) => {
                        this.services = services;
                        if (detail.schedule) {
                          this.program =
                            this.appointmentService.getProgramById(
                              appointmentId,
                              detail.schedule
                            );

                          this.program.serviceobj =
                              this.serviceService.getServiceById(
                                this.program.service || "",
                                services
                              );
                        }
                        this.loading = false;
                      });
              });
          });
      }
    });

    this.rdvstatuses = [
      { label: "Payé", value: this.appointState.PAID },
      { label: "Fait", value: this.appointState.DONE },
      { label: "Annulé", value: this.appointState.CANCELED },
      { label: "En cours", value: this.appointState.ONGOING },
      { label: "Créer", value: this.appointState.CREATED },
    ];
  }

  formatDate(date: string | Date) {
    return "string" == typeof date ? new Date(date) : date;
  }

  calculateHours(minuteValue: number | undefined): number {
    return minuteValue ? Math.floor(minuteValue / 60) : -1;
  }

  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-dd HH:mm")!;
  }

  calculateRemainingMinutes(minuteValue: number | undefined): number {
    return minuteValue ? minuteValue % 60 : -1;
  }
  getRdvStatusByCode(value: number | undefined) {
    return this.rdvstatuses.find((status) => status.value == value);
  }
  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = "";
  }

  done(){
    this.employeService.markAsDone(this.tokenEmploye, side.EMP,this.employe._id||'',this.program._id||'').subscribe(data =>{
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Fait', life: 3000 });
      this.program.state = this.appointState.DONE;
    });
  } 

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }

}
