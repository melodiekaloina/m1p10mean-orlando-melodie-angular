import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Password } from 'primeng/password';
import { stringToSha1 } from 'src/app/helpers/utils';
import { LayoutService } from 'src/app/layouts/bolayouts/service/app.layout.service';
import { Employe } from 'src/app/models/employe.model';
import { EmployeService } from 'src/app/services/employe.service';

@Component({
  selector: 'app-login-employe',
  templateUrl: './login-employe.component.html',
  styleUrls: ['./login-employe.component.scss']
})
export class LoginEmployeComponent {

  employe!: Employe;

  passwordInput!: string;
  password!: string;
  passwordError!: string | undefined;

  email!: string;
  emailError: string | undefined;

  submitted: boolean = false;

  constructor(public layoutService: LayoutService, public messageService: MessageService, private router: Router, private route: ActivatedRoute, private employeService: EmployeService,) {

  }

  ngOnInit() {
    this.email = 'melodiekaloina@gmail.com';
    this.passwordInput = 'kaloina';
    this.password = stringToSha1(this.passwordInput); 
  }
  submit() {
    this.initValue();
    this.submitted = true;
    // control
    if (!this.email || (this.email && this.email.trim() == '')) {
      this.emailError = 'Email est requis.';
      return ;
    }

    if(!this.isValidEmail(this.email)){
      this.emailError = "Format d'email non valide";
      return ;
    }

    if (!this.password) {
      this.passwordError = "Mot de passe requis.";
      return;
    }
    this.email = this.email.trim().toLowerCase();
    this.employeService.login(this.email,this.passwordInput).subscribe(data =>{
      
      // "state": "USER_NOT_FOUND"
      if (data.state == "USER_NOT_FOUND"){
        this.emailError = "e-mail non trouvé";
        return;
      }

      // "state": "WRONG_PASSWORD"
      if (data.state == "WRONG_PASSWORD"){
        this.passwordError = "Mot de passe incorrect";
      }

      //"state": "LOGGED_IN",
      //set user to employe
      if (data.state == "LOGGED_IN" ){
        this.employe = data.user;
        localStorage.setItem('sessionEmp', data.token||'');
        this.router.navigate(['/employe']);

      }

    });

  }

  updatePassword(value: string): void {
    this.password = stringToSha1(value);
    if (this.submitted) {
      this.submitted = false;
      this.initValue();
    }

  }


  initValue() {
    this.emailError = undefined;
    this.passwordError = undefined;

  } 
  
  isValidEmail(email: string): boolean {
    // Regular expression to validate email format
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

}
