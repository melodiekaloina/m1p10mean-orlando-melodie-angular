import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { LayoutService } from 'src/app/layouts/bolayouts/service/app.layout.service';
import { Employe } from 'src/app/models/employe.model';
import { AuthenService } from 'src/app/services/authen.service';
import { EmployeService } from 'src/app/services/employe.service';

@Component({
  selector: 'app-code-password',
  templateUrl: './code-password.component.html',
  styleUrls: ['./code-password.component.scss']
})
export class CodePasswordComponent {
  // if id employe not recognise then redirect not found

  // function call api check idemp and code

  // no error: redirect to edit-password

  // error : 

  employe!: Employe;

  idEmploye!: string;

  code!: number;

  constructor(public layoutService: LayoutService, public messageService: MessageService, private router: Router, private route: ActivatedRoute, private employeService: EmployeService, private authenService: AuthenService,) {

  }
  ngOnInit() {
    this.route.params.subscribe(params => {
      const employeId = params['id'];
      if (employeId) {
        this.employeService.getAllEmployeFree().subscribe(employes => {
          try {
            this.employe = this.employeService.getEmployeById(employeId, employes);
          } catch (error) {
            // redirect 
            this.router.navigate([`/notfound`]);
            return;
          }
          this.idEmploye = employeId;
        })
      }


    });
  }


  // resend email 
  resendEmail() {
    this.authenService.sendEmail(this.employe).subscribe(data => {
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Email Envoyer', life: 3000 });
    },
      error => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Erreur pendant l\'envoye de l\'email', life: 3000 });
      }
    );
  }

  verifyCode() {
    this.authenService.isValid(this.employe.email || '', this.code).subscribe(data => {
      
      this.router.navigate([`/employe/edit-pswrd/${this.idEmploye}`],{state:{allow:true}, relativeTo:this.route});
    },
      error => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Code expire ou invalide', life: 3000 });
      }
    );

  }

  onCodeChange(value: number) {
    this.code = value;

  }
} 
