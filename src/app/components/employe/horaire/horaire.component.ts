import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TimeScale } from "chart.js";
import { MessageService } from "primeng/api";
import { side } from "src/app/helpers/utils";
import { Employe, Workinghour } from "src/app/models/employe.model";
import { Service } from "src/app/models/service.model";
import { AuthenService } from "src/app/services/authen.service";
import { EmployeService } from "src/app/services/employe.service";
import { ServiceService } from "src/app/services/service.service";

@Component({
  selector: "app-horaire",
  templateUrl: "./horaire.component.html",
  styleUrls: ["./horaire.component.css"],
})
export class HoraireComponent {
  tokenEmploye!: string;
  employe!: Employe;
  time: string = "";

  constructor(
    private employeService: EmployeService,
    private messageService: MessageService,
    private authenService: AuthenService,
    private serviceService: ServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.tokenEmploye = localStorage.getItem("sessionEmp") || "";
    this.route.params.subscribe((params) => {
      this.employeService.getEmployeByToken(this.tokenEmploye).subscribe(emp=>{
const employeId = emp._id;
        if (employeId) {
          this.employeService
          .getAllEmploye(this.tokenEmploye, side.EMP)
          .subscribe((employes) => {
            this.employe = this.employeService.getEmployeById(
              employeId,
              employes
            );
            if (!this.employe.workhour || this.employe.workhour.length ==0) {
              this.employeService.getDefaultWorkingHour().then(list => {
                this.employe.workhour = list;                
              });
            }
            this.employe.workhour?.forEach(work=>{
              work.dates = [
                // @ts-ignore
                [this.parseToDate(work.times[0][0]), this.parseToDate(work.times[0][1])],
                // @ts-ignore
                [this.parseToDate(work.times[1][0]), this.parseToDate(work.times[1][1])],
              ];
            });
            
            
          });
        }
      });
    });
  }

  formatDate(date: string | Date) {
    return "string" == typeof date ? new Date(date) : date;
  }

  parseToDate(string:string):Date{
     const [hoursStr, minutesStr] = string.split(':');

     const hours = parseInt(hoursStr, 10);
     const minutes = parseInt(minutesStr, 10);
 
     const currentDate = new Date();
 
     currentDate.setHours(hours);
     currentDate.setMinutes(minutes);
     currentDate.setSeconds(0);
 
     return currentDate;
 }
  
}