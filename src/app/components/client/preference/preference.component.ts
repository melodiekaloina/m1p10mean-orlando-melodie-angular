import { MessageService } from 'primeng/api';

import { Component } from '@angular/core';
import { Client } from 'src/app/models/client.model';
import { Employe } from 'src/app/models/employe.model';
import { Service } from 'src/app/models/service.model';
import { ClientService } from 'src/app/services/client.service';
import { EmployeService } from 'src/app/services/employe.service';
import { ServiceService } from 'src/app/services/service.service';
import { side } from 'src/app/helpers/utils';

interface Product{
  id?:string;
  name?:string
}
@Component({
  selector: 'app-preference',
  templateUrl: './preference.component.html',
  styleUrls: ['./preference.component.css'],
})
export class PreferenceComponent {
  tokenClient!: string;
  client!: Client;

  availableProducts: Product[] | undefined;

  selectedProducts: Product[] | undefined;

  draggedProduct: Product | undefined | null;

  clients!:Client[];

  employes!: Employe[];
  
  availableEmployes: Employe[] | undefined;
  
  selectedEmployes: Employe[] | undefined;

  draggedEmploye: Employe | undefined | null;
  
  services!: Service[];

  availableServices: Service[] | undefined;

  selectedServices: Service[] | undefined;

  draggedService: Service | undefined | null;
  

  constructor(
    private clientService: ClientService,
    private serviceService: ServiceService,
    private employeService: EmployeService,
    private messageService: MessageService
  ) {}
  ngOnInit() {
    this.tokenClient = localStorage.getItem("sessionClient") || "";

      this.clientService.getClientByToken(this.tokenClient).subscribe(client=>{
        this.client = client;
        
        this.employeService.getAllEmploye(this.tokenClient, side.CLIENT).subscribe(employes=>{
          this.employes = employes;

        this.availableEmployes = employes;
          this.selectedEmployes = [];
          this.client.preferences?.employeeReview?.forEach(idEmploye=>{
            this.selectedEmployes?.push(this.employeService.getEmployeById(idEmploye, this.employes));
            this.availableEmployes = this.availableEmployes?.filter((val, i) => val._id != idEmploye);
          });

        this.serviceService.getAllService(this.tokenClient, side.CLIENT).subscribe(services=>{
          this.services = services;

          this.availableServices = services;
          this.selectedServices = [];
          this.client.preferences?.serviceReview?.forEach(idService=>{
            this.selectedServices?.push(this.serviceService.getServiceById(idService, this.services));
            this.availableServices = this.availableServices?.filter((val, i) => val._id != idService);
          });
        });
      });
    });
      this.selectedProducts = [];
      this.availableProducts = [
          {id:'1', name: 'Black Watch'},
          {id:'2', name: 'Bamboo Watch'}
      ]

      // add currentPreference in selected
      // delete on available if already in selected
  }

  dragStart(product: Product) {
      this.draggedProduct = product;
  }
  dragStartEmploye(employe: Employe) {
    this.draggedEmploye = employe;
}
  dragStartService(service: Service) {
    this.draggedService = service;
}

  drop() {
      if (this.draggedProduct) {
          let draggedProductIndex = this.findIndex(this.draggedProduct);
          this.selectedProducts = [...(this.selectedProducts as Product[]), this.draggedProduct];
          this.availableProducts = this.availableProducts?.filter((val, i) => i != draggedProductIndex);
          this.draggedProduct = null;
      }
  }
  dropEmploye() {
    if (this.draggedEmploye && this.draggedEmploye._id) {
      const idDropped = this.draggedEmploye._id;
      
      this.clientService.addFavorite(this.tokenClient, side.CLIENT,this.client._id||'', "employee", idDropped).subscribe(data=>{
        this.client.preferences?.employeeReview?.push(idDropped);
        
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'L\'employé a été ajouté dans vos préférences.', life: 3000 });
        // @ts-ignore
        this.selectedEmployes = [...(this.selectedEmployes as Employe[]), this.draggedEmploye];
        this.availableEmployes = this.availableEmployes?.filter((val, i) => val._id != this.draggedEmploye?._id);
        this.draggedEmploye = null;
      });
    }
} 
  dropService() {
    console.log(this.draggedService);
    if (this.draggedService && this.draggedService._id) {
      const idDropped = this.draggedService._id;
      this.clientService.addFavorite(this.tokenClient, side.CLIENT,this.client._id||'', "service", idDropped).subscribe(data=>{
          this.client.preferences?.serviceReview?.push(idDropped);
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Le service a été ajouté dans vos préférences.', life: 3000 });
      // @ts-ignore
      this.selectedServices = [...(this.selectedServices as Service[]), this.draggedService];
      this.availableServices = this.availableServices?.filter((val, i) => val._id != this.draggedService?._id);
      this.draggedService = null;
          // }
        });
        console.log(this.client);
    }
}

  remove(obj:Product){    
    // get the id from obj 
    let removedProductIndex = this.findIndex(obj);
    // add on availableProduct
    this.availableProducts = [...(this.availableProducts as Product[]), obj];
    // delete on selectedProduct
    
    this.selectedProducts = this.selectedProducts?.filter((val, i) => i != removedProductIndex);
    console.log('available',this.availableProducts);
    console.log('selected',this.selectedProducts);
  }
  
  removeEmploye(obj:Employe){ 
    if(obj._id){
      this.clientService.removeFavorite(this.tokenClient, side.CLIENT,this.client._id||'', "employee", obj._id).subscribe(data=>{
      this.client.preferences!.employeeReview = this.client.preferences?.employeeReview?.filter((val,i)=> val != obj._id);
    this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'L\'employé a été retiré de vos préférences.', life: 3000 });
    // add on availableEmploye
    this.availableEmployes = [...(this.availableEmployes as Employe[]), obj];
    // delete on selectedEmploye
    this.selectedEmployes = this.selectedEmployes?.filter((val, i) => val._id != obj._id);
      });
    }
  }
  removeService(obj:Service){ 
    if(obj._id){
      this.clientService.removeFavorite(this.tokenClient, side.CLIENT,this.client._id||'', "service", obj._id).subscribe(data=>{
      //@ts-ignore
      this.client.preferences!.serviceReview = this.client.preferences?.serviceReview?.filter((val,i)=> val != obj._id);
    this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Le service a été retiré de vos préférences.', life: 3000 });
    // add on availableService
    this.availableServices = [...(this.availableServices as Service[]), obj];
    // delete on selectedService
    this.selectedServices = this.selectedServices?.filter((val, i) => val._id != obj._id);
      });
    }
  }

  dragEndEmploye() {
      this.draggedEmploye = null;
  }
  dragEndService() {
      this.draggedService = null;
  }

  dragEnd() {
      this.draggedProduct = null;
  }

  findIndex(product: Product) {
      let index = -1;
      for (let i = 0; i < (this.availableProducts as Product[]).length; i++) {
          if (product.id === (this.availableProducts as Product[])[i].id) {
              index = i;
              break;
          }
      }
      return index;
  }
 

}
