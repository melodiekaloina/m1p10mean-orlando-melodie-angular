import { NgModule } from "@angular/core";
import { CarouselModule } from 'primeng/carousel';
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ButtonModule } from "primeng/button";
import { DropdownModule } from "primeng/dropdown";
import { InputTextModule } from "primeng/inputtext";
import { ProgressBarModule } from "primeng/progressbar";
import { RatingModule } from "primeng/rating";
import { RippleModule } from "primeng/ripple";
import { TableModule } from "primeng/table";
import { ToastModule } from "primeng/toast";
import { ToggleButtonModule } from "primeng/togglebutton";
import { MultiSelectModule } from 'primeng/multiselect';
import { CalendarModule } from "primeng/calendar";
import { PreferenceComponent } from "./preference.component";
import { DragDropModule } from 'primeng/dragdrop';
import { ChipModule } from 'primeng/chip';
import { MessageService } from "primeng/api";

@NgModule({
    declarations: [PreferenceComponent],
    imports: [
        CarouselModule,
        CommonModule,
		FormsModule,
		TableModule,
		RatingModule,
		ButtonModule,
		InputTextModule,
		ToggleButtonModule,
		RippleModule,     
		MultiSelectModule,
		DropdownModule,
		ProgressBarModule,
		ToastModule,
		CalendarModule,
		DragDropModule ,
		ChipModule 
    ],
    exports: [PreferenceComponent],
})

export default class PreferenceModule { }