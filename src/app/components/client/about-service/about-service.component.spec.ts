import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutServiceComponent } from './about-service.component';

describe('AboutServiceComponent', () => {
  let component: AboutServiceComponent;
  let fixture: ComponentFixture<AboutServiceComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AboutServiceComponent]
    });
    fixture = TestBed.createComponent(AboutServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
