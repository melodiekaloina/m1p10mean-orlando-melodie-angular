import { Component } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { Service } from 'src/app/models/service.model';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-about-service',
  templateUrl: './about-service.component.html',
  styleUrls: ['./about-service.component.css']
})
export class AboutServiceComponent {
  service!: Service;
  services!: Service[];
  showLoader: boolean = true;

  constructor(private serviceService: ServiceService,

    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const serviceId = params['id'];
      // this.onRouteEnter(serviceId);
      this.initServices(serviceId);
      this.hideLoader();
    });
  }

  hideLoader() {
    setTimeout(() => {
      this.showLoader = false; // Hide loader
    }, 2000);
  }

  initServices(serviceId:string): void {
    this.serviceService.getAllServiceFree().subscribe(services => {
      this.services = services;
      this.onRouteEnter(serviceId);
    })
  }

  onRouteEnter(serviceId:any){
    if (serviceId) {
      this.service = this.serviceService.getServiceById(serviceId, this.services);
    } else {
      if (this.services!=undefined && this.services.length != 0) {
        
        this.service = this.serviceService.getServiceById(this.services.at(0)?._id||'-1', this.services);
      }
    }
    
  }
 
  calculateHours(minuteValue:number): number {
    return Math.floor(minuteValue / 60);
  } 

  calculateRemainingMinutes(minuteValue: number): number {
    return minuteValue % 60;
  }
}
