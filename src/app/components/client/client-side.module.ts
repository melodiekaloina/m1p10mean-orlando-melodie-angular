import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientSideRoutingModule } from './client-side-routing.module';
import { HomePageClientComponent } from './home-page-client/home-page-client.component';
import { AboutServiceComponent } from './about-service/about-service.component';
import ClientCommentModule from './client-comment/client.comment.module';

@NgModule({
    declarations: [
        HomePageClientComponent,
        AboutServiceComponent,
    ],
    imports: [
        CommonModule,
        ClientSideRoutingModule,
        ClientCommentModule
    ]
})
export class ClientSideModule { }
