import { DatePipe } from '@angular/common';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Table } from 'primeng/table';
import { side } from 'src/app/helpers/utils';
import { Offer } from 'src/app/models/appointment.model';
import { Client } from 'src/app/models/client.model';
import { ClientService } from 'src/app/services/client.service';
import { OfferService } from 'src/app/services/offer.service';

@Component({
  selector: 'app-list-offre',
  templateUrl: './list-offre.component.html',
  styleUrls: ['./list-offre.component.css']
})
export class ListOffreComponent {
  tokenClient!: string;
  loading: boolean = true;

  client?: Client;

  offers!: Offer[];

  rdvstatuses: any[] = [];


  @ViewChild("filter") filter!: ElementRef;

  constructor(
    private offerService: OfferService, 
    private clientService: ClientService,
    private datePipe: DatePipe,
    private router: Router
  ) {}

  ngOnInit() {
    this.tokenClient = localStorage.getItem("sessionClient") || "";
    this.clientService
      .getClientByToken(this.tokenClient)
      .subscribe((client) => {
        this.client = client;
        this.offerService.getAllOffer(this.tokenClient, side.CLIENT).subscribe(offers=>{
          this.offers = offers;
        });
        this.loading = false;
      });

  }

  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = "";
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }

  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-dd")!;
  }
  getRdvStatusByCode(value: number|undefined) {
    return this.rdvstatuses.find(status => status.value == value);
  }
  redirectToAbout(idoffre: string) {
    this.router.navigate([`/user/about-offre/${idoffre}`]);
  }
    todayDateStr() {
    let today = new Date();

    let year = today.getFullYear().toString();
    let month = (today.getMonth() + 1).toString().padStart(2, "0");
    let day = today.getDate().toString().padStart(2, "0");

    let dateString = `${year}-${month}-${day}`;
    return dateString;
  }
  calculateHours(minuteValue:number|undefined): number {
    return (minuteValue)?Math.floor(minuteValue / 60):-1;
  } 

  calculateRemainingMinutes(minuteValue: number|undefined): number {
    return (minuteValue)?minuteValue % 60:-1;
  }


} 
