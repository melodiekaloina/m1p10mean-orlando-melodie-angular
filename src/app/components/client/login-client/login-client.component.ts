import { Component } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { stringToSha1 } from 'src/app/helpers/utils';
import { Client } from 'src/app/models/client.model';
import { ClientService } from 'src/app/services/client.service';

@Component({
  selector: 'app-login-client',
  templateUrl: './login-client.component.html',
  styleUrls: ['./login-client.component.css']
})
export class LoginClientComponent {
  client!: Client;
  
  showLoader: boolean = true;
  myScriptElement!: HTMLScriptElement;

  passwordInput!: string;
  password!: string;
  passwordError!: string | undefined;

  email!: string;
  emailError: string | undefined;

  submitted: boolean = false;

  constructor( public messageService: MessageService, private router: Router, private route: ActivatedRoute, private clientService: ClientService) {

  }

  ngOnInit() {
    this.showLoader = true; // Show loader on navigation start
    this.hideLoader();   
    this.email = 'andrianayorllando@gmail.com';
    this.passwordInput = '1234'
    this.password = stringToSha1(this.passwordInput) 
  }

  submit() {
    this.initValue();
    this.submitted = true;
    // control
    if (!this.email || (this.email && this.email.trim() == '')) {
      this.emailError = 'Email est requis.';
      return ;
    }

    if(!this.isValidEmail(this.email)){
      this.emailError = "Format d'email non valide";
      return ;
    }

    if (!this.password) {
      this.passwordError = "Mot de passe requis.";
      return;
    }
    this.email = this.email.trim().toLowerCase();
    this.clientService.login(this.email,this.passwordInput).subscribe(data =>{
      
      // "state": "USER_NOT_FOUND"
      if (data.state == "USER_NOT_FOUND"){
        this.emailError = "e-mail non trouvé";
        return;
      }

      // "state": "WRONG_PASSWORD"
      if (data.state == "WRONG_PASSWORD"){
        this.passwordError = "Mot de passe incorrect";
      }

      //"state": "LOGGED_IN",
      //set user to client
      if (data.state == "LOGGED_IN" ){
        this.client = data.user;
        localStorage.setItem('sessionClient', data.token||'');
        this.router.navigate(['/user']);

      }
 
    });

  }

  updatePassword(value: string): void {
    this.password = stringToSha1(value);
    if (this.submitted) {
      this.submitted = false;
      this.initValue();
    }

  }


  initValue() {
    this.emailError = undefined;
    this.passwordError = undefined;

  } 
  
  isValidEmail(email: string): boolean {
    // Regular expression to validate email format
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  
  hideLoader() {
    setTimeout(() => {
      this.showLoader = false; // Hide loader
      this.myScriptElement = document.createElement("script");
      this.myScriptElement.src = "assets/vendor/js/main.js";
      document.body.appendChild(this.myScriptElement);
    }, 2000);
  }



}
