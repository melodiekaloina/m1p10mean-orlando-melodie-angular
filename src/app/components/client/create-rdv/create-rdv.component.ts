import { DatePipe } from "@angular/common";
import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { TimeScale } from "chart.js";
import { MessageService } from "primeng/api";
import { side } from "src/app/helpers/utils";
import { Appointment } from "src/app/models/appointment.model";
import { Client } from "src/app/models/client.model";
import { Employe } from "src/app/models/employe.model";
import { Service } from "src/app/models/service.model";
import { AppointmentService } from "src/app/services/appointment.service";
import { ClientService } from "src/app/services/client.service";
import { EmployeService } from "src/app/services/employe.service";
import { ServiceService } from "src/app/services/service.service";

@Component({
  selector: "app-create-rdv",
  templateUrl: "./create-rdv.component.html",
  styleUrls: ["./create-rdv.component.css"],
})
export class CreateRdvComponent {
  tokenClient: string = "";

  client?: Client;
  appointment: Appointment = {};

  employes?: Employe[];

  services?: Service[];

  selectedService?: Service;

  selectedEmploye?: Employe;

  serviceItems: any[] = [];

  availableEmployes?: Employe[];
  availableEmployeItems: any[] = [];

  constructor(
    private appointmentService: AppointmentService,
    private clientService: ClientService,
    private employeService: EmployeService,
    private serviceService: ServiceService,
    private datePipe: DatePipe,
    private router: Router,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.appointment.programs = [];
    this.tokenClient = localStorage.getItem("sessionClient") || "";
    this.clientService
      .getClientByToken(this.tokenClient)
      .subscribe((client) => {
        this.client = client;
        this.appointment.client = this.client._id;
      });
    this.serviceService
      .getAllService(this.tokenClient, side.CLIENT)
      .subscribe((services) => {
        this.services = services;
        this.services.forEach((service) => {
          this.serviceItems.push({
            label: service.servicename,
            value: service,
          });
        });
      });

    this.employeService
      .getAllEmploye(this.tokenClient, side.CLIENT)
      .subscribe((employes) => (this.employes = employes));
  }

  onChooseService(service: Service) {
    this.employeService
      .getEmployesAvailable(
        this.tokenClient,
        side.CLIENT,
        service._id || "",
        this.appointment.date || ""
      )
      .subscribe((data) => {
        this.availableEmployes = data.data;
        this.availableEmployes?.forEach((availableEmploye) => {
          this.availableEmployeItems = [];
          this.availableEmployeItems.push({
            label: availableEmploye.firstName + "" + availableEmploye.lastName,
            value: availableEmploye,
          });
        });
      });
    // init value selectedEmploye
    this.selectedEmploye = undefined;
  }

  onChooseEmploye(employe: Employe) {
    this.selectedEmploye = employe;
  }

  onDateSelect(value: any) { 
    if (value < new Date()) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'La date de rendez-vous ne peut être antérieure à la date du jour.', life: 3000 });     
      this.appointment.date = undefined; 
    }else{
      this.appointment.date = this.formatDateString(value);
    }
    
    this.appointment.programs = [];
    // init value selectedService, selectedEmploye, availableEmployes, availabeEmployeItems
    this.selectedEmploye = undefined;
    this.selectedService = undefined;
    this.availableEmployes = [];
    this.availableEmployeItems = [];
  }

  addProgram() {
    // if efa misy ao @ list le couple emp&service dia tsy ampina
    const program = {
      employee: this.selectedEmploye?._id,
      service: this.selectedService?._id,
      employeobj: this.selectedEmploye,
      serviceobj: this.selectedService,
    };
    if (!this.alreadyInList(program)) {
      this.appointment.programs?.push(program);
    }
    //init value selectedService, selectedEmploye, availableEmployes, availableEmployeItems
    this.selectedEmploye = undefined;
    this.selectedService = undefined;
    this.availableEmployes = [];
    this.availableEmployeItems = [];
  }

  removeProgram(program: any) {
    let removeIndex = this.findIndex(program);
    this.appointment.programs = this.appointment.programs?.filter(
      (val, i) => i != removeIndex
    );
    console.log(this.appointment.programs);
  }
  findIndex(program: any) {
    let index = -1;
    for (let i = 0; i < this.appointment.programs!.length; i++) {
      if (
        program.employee == this.appointment.programs![i].employee &&
        program.service == this.appointment.programs![i].service
      ) {
        index = i;
        break;
      }
    }
    return index;
  }
  alreadyInList(program: any): boolean {
    for (let i = 0; i < this.appointment.programs!.length; i++) {
      if (
        program.employee == this.appointment.programs![i].employee &&
        program.service == this.appointment.programs![i].service
      ) {
        return true;
      }
    }
    return false;
  }
  create() {
    if(!this.appointment.date){
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Veuillez sélectionner la date et l\'heure de votre rendez-vous.', life: 3000 });
      return;
    }
    if(!this.appointment.programs||this.appointment.programs?.length==0){
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Il est obligatoire d\'ajouter un ou plusieurs programmes.', life: 3000 });
      return ;      
  }  
    // create appointment
      this.appointmentService.addAppointment(this.tokenClient, side.CLIENT,this.appointment).subscribe(data=>{
        //redirect 
        this.router.navigate([`/user`]);
      },
        error=>{
          if(error.status == 500){
            this.messageService.add({ severity: 'error', summary: 'Error', detail: error.error.data.message, life: 3000 });
          }
        } 
      )
  }
  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-ddTHH:mm")!;
  }
  formatDate(date: string) {
    return new Date(date);
  }
  todayDateStr() {
    let today = new Date();

    let year = today.getFullYear().toString();
    let month = (today.getMonth() + 1).toString().padStart(2, "0");
    let day = today.getDate().toString().padStart(2, "0");

    let dateString = `${year}-${month}-${day}`;
    return dateString;
  }
}

