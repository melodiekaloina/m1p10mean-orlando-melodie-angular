import { ClientService } from 'src/app/services/client.service';
import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Client } from 'src/app/models/client.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {
  showLoader: boolean = true;
  myScriptElement!: HTMLScriptElement;

  client!: Client;

  emailError: string | undefined;

  errorField!: any[]; 


  constructor(private router: Router,private clientService: ClientService) {
    this.errorField = [
      { field: "firstName", header: "Nom" },
      { field: "lastName", header: "Prenom" },
      { field: 'password', header: "Mot de passe" },
      { field: 'phoneNumber', header: "Numero" },
    ];
    this.client = {};
  } 

  ngOnInit() {
        this.showLoader = true; // Show loader on navigation start
        this.hideLoader();
      
  }

  submit(){
    console.log('object');
    this.initValue();
    if (!this.client.email || (this.client.email && this.client.email.trim() == '')) {
      this.emailError = 'Email est requis.';
    }
    if(this.client.email && !this.isValidEmail(this.client.email)){
      this.emailError = "Format d'email non valide";
    }

    var requiredInvalid = false;
    this.errorField.forEach(field => {
      const fieldValue = (this.client as any)[field.field];
      if (fieldValue == undefined || fieldValue == '') {
        this.setErrorForField(field.field, field.header + ' est requis.');
        requiredInvalid = true;
      }
    });
    if (requiredInvalid || this.emailError) {
      return;
    }
    if(this.client.phoneNumber && this.client.phoneNumber?.length<6){
      this.setErrorForField('phoneNumber', 'le numéro doit comporter 6 chiffres')
      return;
    }
    this.clientService.addClient(this.client).subscribe(data =>{
      this.router.navigate([`/user`]);
    });

  }

  initValue() {
    //init errorField
    this.errorField.forEach(field => {
      this.setErrorForField(field.field, undefined);
    });
  }
  getErrorByFieldName(fieldName: string) {
    return this.errorField.find(field => field.field === fieldName);
  }

  isValidEmail(email: string): boolean {
    // Regular expression to validate email format
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  }

  setErrorForField(fieldName: string, errorMessage: string | undefined): void {
    const fieldObj = this.errorField.find(field => field.field === fieldName);
    if (fieldObj) {
      fieldObj.error = errorMessage;
    }
  }

  hideLoader() {
    setTimeout(() => {
      this.showLoader = false; // Hide loader
      this.myScriptElement = document.createElement("script");
      this.myScriptElement.src = "assets/vendor/js/main.js";
      document.body.appendChild(this.myScriptElement);
    }, 2000);
  }
  

}
