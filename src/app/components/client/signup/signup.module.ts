import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { FormsModule } from '@angular/forms';
import { PasswordModule } from 'primeng/password';
import { InputTextModule } from 'primeng/inputtext';
import { RouterModule } from '@angular/router';
import { ToastModule } from 'primeng/toast';
import { ClientService } from 'src/app/services/client.service';
import { SignupComponent } from './signup.component';

@NgModule({
    imports: [
        CommonModule,
        ButtonModule,
        CheckboxModule,
        InputTextModule,
        FormsModule,
        PasswordModule, 
        RouterModule,
        ToastModule,
        
    ],
    declarations: [SignupComponent],
    providers: [ ClientService ]
})
export class SignupModule { }
