import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientUserSideRoutingModule } from './client-user-side-routing.module';
import ListRdvModule from './list-rdv/list-rdv.module';
import PreferenceModule from './preference/preference.module';
import { AboutAppointModule } from './about-appoint/about-appoint.module';
import { CreateRdvModule } from './create-rdv/create-rdv.module';
import AboutOffreModule from './about-offre/about-offre.module';
import ListOffreModule from './list-offre/list-offre.module';

@NgModule({
    declarations: [
    ],
    imports: [
        CommonModule,
        ClientUserSideRoutingModule,
        ListRdvModule,
        PreferenceModule,
        AboutAppointModule,
        CreateRdvModule,
        ListOffreModule,
        AboutOffreModule
    ]
})
export class ClientUserSideModule { }
