import { Component } from '@angular/core';
import { Service } from 'src/app/models/service.model';
import { ServiceService } from 'src/app/services/service.service';

@Component({
  selector: 'app-home-page-client',
  templateUrl: './home-page-client.component.html',
  styleUrls: ['./home-page-client.component.css']
})
export class HomePageClientComponent {
  services !: Service[];
  showLoader: boolean = true;

  constructor(private serviceService: ServiceService) { }

  ngOnInit(){
    this.serviceService.getAllServiceFree().subscribe(services => {
      this.services = services;
      this.hideLoader();
    })
  }

  hideLoader() {
    setTimeout(() => {
      this.showLoader = false; // Hide loader
    }, 2000);
  }
 
}
