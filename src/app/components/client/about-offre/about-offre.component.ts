import { DatePipe } from "@angular/common";
import { Component, ElementRef, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { Table } from "primeng/table";
import { side } from "src/app/helpers/utils";
import {
  Appointment,
  Offer,
  ServiceOffer,
} from "src/app/models/appointment.model";
import { Client } from "src/app/models/client.model";
import { Employe } from "src/app/models/employe.model";
import { Service } from "src/app/models/service.model";
import { ClientService } from "src/app/services/client.service";
import { EmployeService } from "src/app/services/employe.service";
import { OfferService } from "src/app/services/offer.service";
import { ServiceService } from "src/app/services/service.service";

@Component({
  selector: "app-about-offre",
  templateUrl: "./about-offre.component.html",
  styleUrls: ["./about-offre.component.css"],
})
export class AboutOffreComponent {
  tokenClient!: string;
  offer!: Offer;

  client!: Client;

  employes?: Employe[];

  services?: Service[];

  selectedService?: Service;

  selectedEmploye?: Employe;

  serviceItems: any[] = [];

  availableEmployes?: Employe[];
  availableEmployeItems: any[] = [];

  rdvstatuses: any[] = [];

  display: boolean = false;


  @ViewChild("filter") filter!: ElementRef;

  appointment: Appointment = {};

  loading: boolean = true;
  constructor(
    private offerService: OfferService,
    private clientService: ClientService,
    private employeService: EmployeService,
    private serviceService: ServiceService,
    private messageService: MessageService,
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.appointment.programs = [];
    this.tokenClient = localStorage.getItem("sessionClient") || "";
    this.route.params.subscribe((params) => {
      const offerId = params["id"];

      if (offerId) {
        this.employeService
          .getAllEmploye(this.tokenClient, side.CLIENT)
          .subscribe((employes) => (this.employes = employes));
        this.clientService
          .getClientByToken(this.tokenClient)
          .subscribe((client) => {
            this.client = client;
            this.appointment.client = this.client._id;
            this.clientService
              .getDetailClient(this.tokenClient, side.CLIENT, client._id || "")
              .subscribe((detail) => {
                this.serviceService
                  .getAllServiceFree()
                  .subscribe((services) => {
                    this.services = services;
                    this.offerService
                      .getAllOffer(this.tokenClient, side.CLIENT)
                      .subscribe((offers) => {
                        const offer = this.offerService.getOfferById(
                          offerId,
                          offers
                        );
                        if (offer) {
                          this.offer = offer;

                          this.serviceItems = [];
                          (this.offer.services as ServiceOffer[]).forEach(
                            (serviceoffer) => {
                              serviceoffer.serviceobj =
                                this.serviceService.getServiceById(
                                  serviceoffer.service || "",
                                  this.services ||[]
                                );
                                this.serviceItems.push({
                                  label: serviceoffer.serviceobj.servicename,
                                  value: serviceoffer.serviceobj,
                                });
                            }
                          );
                        }
                        this.loading = false;
                      });
                  });
              });
          });
      }
    });
  }

  onChooseService(service: Service) {
    this.employeService
      .getEmployesAvailable(
        this.tokenClient,
        side.CLIENT,
        service._id || "",
        this.appointment.date || ""
      )
      .subscribe((data) => {
        this.availableEmployes = data.data;
        this.availableEmployes?.forEach((availableEmploye) => {
          this.availableEmployeItems = [];
          this.availableEmployeItems.push({
            label: availableEmploye.firstName + "" + availableEmploye.lastName,
            value: availableEmploye,
          });
        });
      });
    // init value selectedEmploye
    this.selectedEmploye = undefined;
  }

  onChooseEmploye(employe: Employe) {
    this.selectedEmploye = employe;
  }

  onDateSelect(value: any) { 
    const datestart = new Date(this.offer.start||'');
    const dateend = new Date(this.offer.end||'');
    if (value < new Date()) {
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'La date de rendez-vous ne peut être antérieure à la date du jour.', life: 3000 });     
      this.appointment.date = undefined; 
    }
    else if(value<datestart || value > dateend){
      this.messageService.add({ severity: 'error', summary: 'Error', detail: `La date doit être entre ${this.formatDateString(datestart)} et ${this.formatDateString(dateend)}.`, life: 3000 });     
      this.appointment.date = undefined; 
    }else{
      this.appointment.date = this.formatDateString(value);
    }
    
    this.appointment.programs = [];
    // init value selectedService, selectedEmploye, availableEmployes, availabeEmployeItems
    this.selectedEmploye = undefined;
    this.selectedService = undefined;
    this.availableEmployes = [];
    this.availableEmployeItems = [];
  }

  addProgram() {
    // if efa misy ao @ list le couple emp&service dia tsy ampina
    const program = {
      employee: this.selectedEmploye?._id,
      service: this.selectedService?._id,
      employeobj: this.selectedEmploye,
      serviceobj: this.selectedService,
    };
    if (!this.alreadyInList(program)) {
      this.appointment.programs?.push(program);
    }
    //init value selectedService, selectedEmploye, availableEmployes, availableEmployeItems
    this.selectedEmploye = undefined;
    this.selectedService = undefined;
    this.availableEmployes = [];
    this.availableEmployeItems = [];
  }

  removeProgram(program: any) {
    let removeIndex = this.findIndex(program);
    this.appointment.programs = this.appointment.programs?.filter(
      (val, i) => i != removeIndex
    );
    console.log(this.appointment.programs);
  }
  findIndex(program: any) {
    let index = -1;
    for (let i = 0; i < this.appointment.programs!.length; i++) {
      if (
        program.employee == this.appointment.programs![i].employee &&
        program.service == this.appointment.programs![i].service
      ) {
        index = i;
        break;
      }
    }
    return index;
  }
  alreadyInList(program: any): boolean {
    console.log(this.appointment.programs!.length);
    for (let i = 0; i < this.appointment.programs!.length; i++) {
      console.log(program, this.appointment.programs);
      if (
        program.employee == this.appointment.programs![i].employee &&
        program.service == this.appointment.programs![i].service
      ) {
        return true;
      }
    }
    return false;
  }
  create() {
    if(!this.appointment.date){
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Veuillez sélectionner la date et l\'heure de votre rendez-vous.', life: 3000 });
      return;
    }
    if(!this.appointment.programs||this.appointment.programs?.length==0){
      this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Il est obligatoire d\'ajouter un ou plusieurs programmes.', life: 3000 });
      return ;      
  }  
    // create appointment
      this.appointment.date = this.appointment.date.replace(' ', 'T');
      this.offerService.takeOffer(this.tokenClient, side.CLIENT,this.offer._id||'', this.appointment).subscribe(data=>{
        //   //redirect 
          this.router.navigate([`/user`]);
        },
          error=>{
            if(error.status == 500){
              this.messageService.add({ severity: 'error', summary: 'Error', detail: error.error.data.message, life: 3000 });
            }
          } 
        )


  }

  formatDate(date: string | Date) {
    return "string" == typeof date ? new Date(date) : date;
  }

  calculateHours(minuteValue: number | undefined): number {
    return minuteValue ? Math.floor(minuteValue / 60) : -1;
  }

  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-dd HH:mm")!;
  }

  calculateRemainingMinutes(minuteValue: number | undefined): number {
    return minuteValue ? minuteValue % 60 : -1;
  }
  getRdvStatusByCode(value: number | undefined) {
    return this.rdvstatuses.find((status) => status.value == value);
  }
  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = "";
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }
}
