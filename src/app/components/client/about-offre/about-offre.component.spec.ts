import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutOffreComponent } from './about-offre.component';

describe('AboutOffreComponent', () => {
  let component: AboutOffreComponent;
  let fixture: ComponentFixture<AboutOffreComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AboutOffreComponent]
    });
    fixture = TestBed.createComponent(AboutOffreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
