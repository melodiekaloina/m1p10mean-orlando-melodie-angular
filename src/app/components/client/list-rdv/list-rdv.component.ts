import { AppointmentService } from "./../../../services/appointment.service";

import { Component, ElementRef, ViewChild } from "@angular/core";
import { Customer, Representative } from "../../bocomponents/api/customer";
import { Table } from "primeng/table";
import { Appointment } from "src/app/models/appointment.model";
import { Employe } from "src/app/models/employe.model";
import { EmployeService } from "src/app/services/employe.service";
import { Service } from "src/app/models/service.model";
import { ServiceService } from "src/app/services/service.service";
import { MessageService, ConfirmationService } from "primeng/api";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";
import { ClientService } from "src/app/services/client.service";
import { appointmentState, side } from "src/app/helpers/utils";
import { Client, AppointClient } from "src/app/models/client.model";

@Component({
  selector: "app-list-rdv",
  templateUrl: "./list-rdv.component.html",
  styleUrls: ["./list-rdv.component.css"],
  providers: [MessageService, ConfirmationService, DatePipe],
})
export class ListRdvComponent {
  tokenClient!: string;
  loading: boolean = true;

  client?: Client;

  appointments!: AppointClient[];

  employes!: Employe[];

  services!: Service[];

  rdvstatuses: any[] = [];

  appointState = appointmentState;

  @ViewChild("filter") filter!: ElementRef;

  constructor(
    private appointmentService: AppointmentService,
    private employeService: EmployeService,
    private clientService: ClientService,
    private serviceService: ServiceService,
    private datePipe: DatePipe,
    private router: Router
  ) {}

  ngOnInit() {
    this.tokenClient = localStorage.getItem("sessionClient") || "";
    this.clientService
      .getClientByToken(this.tokenClient)
      .subscribe((client) => {

        this.clientService.getDetailClient(this.tokenClient, side.CLIENT, client._id||'').subscribe(detail=>{
          this.client = detail; 
          if(this.client.appointments){
            this.appointments = this.client.appointments ;
          }
        });
        this.loading = false;
      });

      this.rdvstatuses = [
        { label: "Payé", value: this.appointState.PAID },
        { label: "Fait", value: this.appointState.DONE },
        { label: "Annulé", value: this.appointState.CANCELED },
        { label: "En cours", value: this.appointState.ONGOING },
        { label: "Créer", value: this.appointState.CREATED }
      ];
  }

  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = "";
  }

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }

  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-dd")!;
  }
  getRdvStatusByCode(value: number|undefined) {
    return this.rdvstatuses.find(status => status.value == value);
  }
  redirectToAboutAppointClient(idAppointment: string) {
    this.router.navigate([`/user/about-appoint/${idAppointment}`]);
  }
}
