import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutAppointComponent } from './about-appoint.component';

describe('AboutAppointComponent', () => {
  let component: AboutAppointComponent;
  let fixture: ComponentFixture<AboutAppointComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AboutAppointComponent]
    });
    fixture = TestBed.createComponent(AboutAppointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
