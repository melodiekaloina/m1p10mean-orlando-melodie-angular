import { Component, ElementRef, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { Appointment } from "src/app/models/appointment.model";
import { AppointmentService } from "src/app/services/appointment.service";
import { AuthenService } from "src/app/services/authen.service";
import { ClientService } from "src/app/services/client.service";
import { EmployeService } from "src/app/services/employe.service";
import { ServiceService } from "src/app/services/service.service";
import { appointmentState, side } from "src/app/helpers/utils";
import { Employe } from "src/app/models/employe.model";
import { Service } from "src/app/models/service.model";
import { DatePipe } from "@angular/common";
import { Table } from "primeng/table";
import { Client } from "src/app/models/client.model";

@Component({
  selector: "app-about-appoint",
  templateUrl: "./about-appoint.component.html",
  styleUrls: ["./about-appoint.component.css"],
})
export class AboutAppointComponent {
  tokenClient!: string;
  appointment!: Appointment;

  client!: Client;

  rdvstatuses: any[] = [];

  display: boolean = false;

  appointState = appointmentState;

  totalPrice!: number;

  employes!: Employe[];

  services!: Service[];

  @ViewChild("filter") filter!: ElementRef;

  loading: boolean = true;

  constructor(
    private appointmentService: AppointmentService,
    private employeService: EmployeService,
    private clientService: ClientService,
    private serviceService: ServiceService,
    private messageService: MessageService,
    private authenService: AuthenService,
    private datePipe: DatePipe,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.tokenClient = localStorage.getItem("sessionClient") || "";
    this.route.params.subscribe((params) => {
      const appointmentId = params["id"];

      if (appointmentId) {
        this.clientService
          .getClientByToken(this.tokenClient)
          .subscribe((client) => {
            this.client = client;
            this.clientService
              .getDetailClient(this.tokenClient, side.CLIENT, client._id || "")
              .subscribe((detail) => {
                this.employeService
                  .getAllEmploye(this.tokenClient, side.CLIENT)
                  .subscribe((employes) => {
                    this.employes = employes;
                    this.serviceService
                      .getAllServiceFree()
                      .subscribe((services) => {
                        this.services = services;
                        this.totalPrice = 0;
                        if (detail.appointments) {
                          this.appointment =
                            this.appointmentService.getAppointById(
                              appointmentId,
                              detail.appointments
                            );

                          this.appointment.programs?.forEach((program) => {
                            this.totalPrice += program.price || 0;
                            program.employeobj =
                              this.employeService.getEmployeById(
                                program.employee || "",
                                employes
                              );
                            program.serviceobj =
                              this.serviceService.getServiceById(
                                program.service || "",
                                services
                              );
                          });
                        }
                        this.loading = false;
                      });
                  });
              });
          });
      }
    });

    this.rdvstatuses = [
      { label: "Payé", value: this.appointState.PAID },
      { label: "Fait", value: this.appointState.DONE },
      { label: "Annulé", value: this.appointState.CANCELED },
      { label: "En cours", value: this.appointState.ONGOING },
      { label: "Créer", value: this.appointState.CREATED },
    ];
  }

  formatDate(date: string | Date) {
    return "string" == typeof date ? new Date(date) : date;
  }

  calculateHours(minuteValue: number | undefined): number {
    return minuteValue ? Math.floor(minuteValue / 60) : -1;
  }

  formatDateString(date: Date): string {
    return this.datePipe.transform(date, "yyyy-MM-dd HH:mm")!;
  }

  calculateRemainingMinutes(minuteValue: number | undefined): number {
    return minuteValue ? minuteValue % 60 : -1;
  }
  getRdvStatusByCode(value: number | undefined) {
    return this.rdvstatuses.find((status) => status.value == value);
  }
  clear(table: Table) {
    table.clear();
    this.filter.nativeElement.value = "";
  }

  pay(){
    this.clientService.pay(this.tokenClient, side.CLIENT,this.client._id||'',this.appointment._id||'').subscribe(data =>{
      this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Payer avec success', life: 3000 });
      this.appointment.state = this.appointState.PAID;
    });
  } 

  onGlobalFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as HTMLInputElement).value, "contains");
  }
}
