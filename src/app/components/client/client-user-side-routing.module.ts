import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomePageClientComponent } from './home-page-client/home-page-client.component';
import { ListRdvComponent } from './list-rdv/list-rdv.component';
import { SignupComponent } from './signup/signup.component';
import { PreferenceComponent } from './preference/preference.component';
import { AboutAppointComponent } from './about-appoint/about-appoint.component';
import { CreateRdvComponent } from './create-rdv/create-rdv.component';
import { ListOffreComponent } from './list-offre/list-offre.component';
import { AboutOffreComponent } from './about-offre/about-offre.component';

@NgModule({
    imports: [RouterModule.forChild([
        { path: "", component: ListRdvComponent },
        { path: "create-rdv", component: CreateRdvComponent },
        { path: "about-appoint/:id", component: AboutAppointComponent },
        { path: "preference", component: PreferenceComponent },
        { path: 'list-offre', component: ListOffreComponent },
        { path: 'about-offre/:id', component: AboutOffreComponent },
        
        
        // { path: '**', redirectTo: '/notfound' }
    ])], 
    exports: [RouterModule]
})
export class ClientUserSideRoutingModule { }
