import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomePageClientComponent } from './home-page-client/home-page-client.component';
import { AboutServiceComponent } from './about-service/about-service.component';

@NgModule({
    imports: [RouterModule.forChild([
        { path: "", component: HomePageClientComponent },
        { path: "services", component: AboutServiceComponent },
        { path: "services/:id", component: AboutServiceComponent },
        
        // { path: '**', redirectTo: '/notfound' }
    ])], 
    exports: [RouterModule]
})
export class ClientSideRoutingModule { }
