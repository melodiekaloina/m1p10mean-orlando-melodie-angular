import { Component } from '@angular/core';
import { ClientComment } from 'src/app/models/client-comment.model';
import { ClientService } from 'src/app/services/client.service';

@Component({
  selector: 'app-client-comment',
  templateUrl: './client-comment.component.html',
  styleUrls: ['./client-comment.component.css']
})
export class ClientCommentComponent {
  clientcomments !: ClientComment[];
  responsiveOptions!: any[];
  constructor(private clientService: ClientService) { }
 
  ngOnInit(){
    this.clientcomments = this.clientService.getAllClientComment();

    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1
      }
    ];
  }

}
