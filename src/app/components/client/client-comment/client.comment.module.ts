import { NgModule } from "@angular/core";
import { ClientCommentComponent } from "./client-comment.component";
import { CarouselModule } from 'primeng/carousel';

@NgModule({
    declarations: [ClientCommentComponent],
    imports: [
        CarouselModule
    ],
    exports: [ClientCommentComponent]
})

export default class ClientCommentModule { }