import { Component } from "@angular/core";
import FetchHelper from "src/app/helpers/FetchHelper";
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import {HttpClient} from '@angular/common/http';
import { FormGroup,FormBuilder,Validators } from "@angular/forms";


@Component({
  selector:"login",
  templateUrl:"./Login.component.html",
  providers:[MessageService]
})

export default class LoginComponent{
  
  loginForm:FormGroup;
  
  constructor(private messageService: MessageService,
    private router: Router,
    private http:HttpClient,
    private fb:FormBuilder) {
      this.loginForm=this.fb.group({
        identifiant:['',Validators.required],
        password:['',Validators.required]
      })
  }

  login(){
    if(this.loginForm.invalid){
      return;
    }

    this.http.post(FetchHelper.setUrl("admin/login"),this.loginForm.value,{
      headers:{
        'Content-Type':'application/json'
      }
    })
    .subscribe((response:any)=>{
      if(response.error!=null){      
        this.messageService.add({severity:"error",summary:"Erreur",detail:response.message});
      } 
      else{
        this.router.navigate(['/home']);
      }
    })

  }



}
