import { NgModule } from "@angular/core";
import LoginComponent from "./Login.component";
import { ToastModule } from 'primeng/toast';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations:[LoginComponent],
    imports:[
        ToastModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ],
    exports:[LoginComponent]
})

export default class LoginModule{}