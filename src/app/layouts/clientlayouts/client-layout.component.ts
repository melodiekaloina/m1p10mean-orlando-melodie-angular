import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-client-layout',
  templateUrl: './client-layout.component.html',
  styleUrls: ['./client-layout.component.css']
})
export class ClientLayoutComponent {
  showLoader: boolean = true;
  myScriptElement!: HTMLScriptElement;

  constructor(private router: Router) {
    
    }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.showLoader = true; // Show loader on navigation start
        this.hideLoader();
      }
    });
  }

  hideLoader() {
    setTimeout(() => {
      this.showLoader = false; // Hide loader
      this.myScriptElement = document.createElement("script");
      this.myScriptElement.src = "assets/vendor/js/main.js";
      document.body.appendChild(this.myScriptElement);
    }, 2000);
  }

  
}
