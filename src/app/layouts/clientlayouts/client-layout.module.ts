import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClientLayoutComponent } from './client-layout.component';
import { SidebarClientComponent } from './sidebar-client/sidebar-client.component';
import { FooterClientComponent } from './footer-client/footer-client.component';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        SidebarClientComponent,
        FooterClientComponent,
        ClientLayoutComponent,        
        
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule
    ],
    exports: [ClientLayoutComponent]
})
export class ClientLayoutModule { }
