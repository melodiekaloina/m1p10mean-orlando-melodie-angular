import { Component, ElementRef, Input, ViewChild } from "@angular/core";
import { MenuItem } from "primeng/api";
import { LayoutService } from "./service/app.layout.service";
import { EmployeService } from "src/app/services/employe.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Employe } from "src/app/models/employe.model";
import { Admin } from "src/app/models/admin.model";
import { AdminService } from "src/app/services/admin.service";

@Component({
  selector: "app-topbar",
  templateUrl: "./app.topbar.component.html",
})
export class AppTopBarComponent {
  @Input() userType!: string;

  topTitle: string = "";

  items!: MenuItem[];

  @ViewChild("menubutton") menuButton!: ElementRef;

  @ViewChild("topbarmenubutton") topbarMenuButton!: ElementRef;

  @ViewChild("topbarmenu") menu!: ElementRef;

  employe!: Employe;

  admin!: Admin;

  constructor(
    public layoutService: LayoutService,
    private router: Router,
    private route: ActivatedRoute,
    private employeService: EmployeService,
    private adminService: AdminService,
  ) {}

  ngOnInit() {
    if (this.userType === "admin") {
      this.topTitle = "Admin";
      const sessionString = localStorage.getItem("sessionAdmin");
      if (sessionString == "" || sessionString == null) {
        this.router.navigate(["/employe/login"]);
        return;
      } else {
        const tokenAdmin = sessionString;    
          try {
            this.adminService
              .getAdminByToken(tokenAdmin)
              .subscribe((admin) => {
                if (!admin) {
                  localStorage.removeItem("sessionAdmin");
                  this.router.navigate(["/admin/login"]);
                  return;
                }
                this.admin = admin;
              });
          } catch (error) {
            localStorage.removeItem("sessionAdmin");
            this.router.navigate(["/admin/login"]);
            return;
          }
      }

    } else {
      const sessionString = localStorage.getItem("sessionEmp");
      if (sessionString == "" || sessionString == null) {
        this.router.navigate(["/employe/login"]);
        return;
      } else {
        const tokenEmp = sessionString;    
          try {
            this.employeService
              .getEmployeByToken(tokenEmp)
              .subscribe((employe) => {
                if (!employe) {
                  localStorage.removeItem("sessionEmp");
                  this.router.navigate(["/employe/login"]);
                  return;
                }
                this.employe = employe;
              });
          } catch (error) {
            localStorage.removeItem("sessionEmp");
            this.router.navigate(["/employe/login"]);
            return;
          }
      }

    }
  }
}
