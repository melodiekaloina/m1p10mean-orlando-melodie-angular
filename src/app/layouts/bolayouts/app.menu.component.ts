import { Input, OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { LayoutService } from './service/app.layout.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {
    @Input() userType!: string;

    model !: any;

    constructor(public layoutService: LayoutService) { }

    ngOnInit() {
        this.model = {
            'admin': [
                {
                    label: 'Statistique',
                    items: [
                        // { label: 'Home', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin'] },
                        { label: 'Réservation', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/'] },
                        { label: 'Chiffre d\'affaire', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/earning'] },
                        { label: 'Benefice', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/benefit'] },
                        { label: 'Temp moyen travail Employe', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/avgwork'] },
                    ]
                },
                {
                    label: 'Employe',
                    items: [
                        { label: 'Gestion Employe', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/crud/employe'] }
                    ]
                },
                {
                    label: 'Service',
                    items: [
                        { label: 'Gestion Service', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/crud/service'] }
                    ]
                },
                {
                    label: 'Dépense',
                    items: [
                        { label: 'Gestion type dépense', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/crud/spendingtype'] },
                        { label: 'Gestion dépense', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/crud/spending'] }
                    ]
                },
                {
                    label: 'Offre special',
                    items: [
                        { label: 'Ajout offre', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/create-offre'] },
                        { label: 'List', icon: 'pi pi-fw pi-circle-off', routerLink: ['/admin/list-offre'] }
                    ]
                },
                // {
                //     label: 'Demo',
                //     icon: 'pi pi-fw pi-briefcase',
                //     items: [
                //         { label: 'Tableau de bord', icon: 'pi pi-fw pi-home', routerLink: ['/admin/dashboard'] },
                //         {
                //             label: 'Crud',
                //             icon: 'pi pi-fw pi-pencil',
                //             routerLink: ['/admin/demo/crud']
                //         },
                //         {
                //             label: 'Timeline',
                //             icon: 'pi pi-fw pi-calendar',
                //             routerLink: ['/admin/demo/timeline']
                //         },
                //         {
                //             label: 'Not Found',
                //             icon: 'pi pi-fw pi-exclamation-circle',
                //             routerLink: ['/notfound']
                //         },
                //         {
                //             label: 'Empty',
                //             icon: 'pi pi-fw pi-circle-off',
                //             routerLink: ['/admin/demo/empty']
                //         },
                //     ]
                // },
            ],

            'emp': [
                {
                    label: 'Accueil',
                    items: [
                        { label: 'Taches', icon: 'pi pi-fw pi-home', routerLink: ['/employe'] },
                        { label: 'Horaire', icon: 'pi pi-fw pi-home', routerLink: ['/employe/horaire'] }
                    ]
                },
            ]

        };

        // console.log(this.model[this.userType]);
    }
}
 