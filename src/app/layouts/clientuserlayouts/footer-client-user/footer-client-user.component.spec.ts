import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterClientUserComponent } from './footer-client-user.component';

describe('FooterClientUserComponent', () => {
  let component: FooterClientUserComponent;
  let fixture: ComponentFixture<FooterClientUserComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FooterClientUserComponent]
    });
    fixture = TestBed.createComponent(FooterClientUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
