import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterClientComponent } from './../clientlayouts/footer-client/footer-client.component';
import { RouterModule } from '@angular/router';
import { ClientUserLayoutComponent } from './client-user-layout.component';
import { SidebarClientUserComponent } from './sidebar-client-user/sidebar-client-user.component';
import { FooterClientUserComponent } from './footer-client-user/footer-client-user.component';

@NgModule({
    declarations: [
        SidebarClientUserComponent,
        FooterClientUserComponent,
        ClientUserLayoutComponent,                
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule 
    ],
    exports: [ClientUserLayoutComponent]
})
export class ClientUserLayoutModule { }
