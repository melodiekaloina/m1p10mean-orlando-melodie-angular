import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Client } from 'src/app/models/client.model';
import { ClientService } from 'src/app/services/client.service';

@Component({
  selector: 'app-sidebar-client-user',
  templateUrl: './sidebar-client-user.component.html',
  styleUrls: ['./sidebar-client-user.component.css']
})
export class SidebarClientUserComponent {
  client!: Client;
  constructor(private router: Router, private route: ActivatedRoute, private clientService: ClientService,) { }

  ngOnInit() {
    const sessionString = localStorage.getItem('sessionClient');
    if (sessionString == '' || sessionString == null) {
      this.router.navigate(['/user/login']);
      return;
    }

    var tokenClient: string = localStorage.getItem("sessionClient") || '';    
    this.clientService.getClientByToken(tokenClient).subscribe((client) => {
      if (!client) {
        localStorage.removeItem("sessionClient");
        this.router.navigate(["/user/login"]);
        return ;
      } 
      else{
        this.client = client;
      }
    }, (error) => {
      
      localStorage.removeItem("sessionClient");
      this.router.navigate(["/user/login"]);
      return;
    });
  }

  logout(){
    localStorage.removeItem('sessionClient');
    this.router.navigate(['/']);
  }


}
