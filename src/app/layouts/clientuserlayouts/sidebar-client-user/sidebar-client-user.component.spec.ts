import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarClientUserComponent } from './sidebar-client-user.component';

describe('SidebarClientUserComponent', () => {
  let component: SidebarClientUserComponent;
  let fixture: ComponentFixture<SidebarClientUserComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SidebarClientUserComponent]
    });
    fixture = TestBed.createComponent(SidebarClientUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
