import { NgModule } from "@angular/core";
import HomeComponent from "./Home.component";
@NgModule({
    declarations:[HomeComponent],
    imports:[],
    exports:[HomeComponent]
})

export default class HomeModule{}
