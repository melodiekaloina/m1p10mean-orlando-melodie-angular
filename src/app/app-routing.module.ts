import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import LoginComponent from './layouts/auth/Login.component';
import HomeComponent from './layouts/home/Home.component';
import { AppLayoutComponent } from './layouts/bolayouts/app.layout.component';
import { NotfoundComponent } from './components/bocomponents/notfound/notfound.component';
import { ClientLayoutComponent } from './layouts/clientlayouts/client-layout.component';
import { CodePasswordComponent } from './components/employe/code-password/code-password.component';
import { EditPasswordComponent } from './components/employe/edit-password/edit-password.component';
import { LoginEmployeComponent } from './components/employe/login-employe/login-employe.component';
import { ClientUserLayoutComponent } from './layouts/clientuserlayouts/client-user-layout.component';
import { SignupComponent } from './components/client/signup/signup.component';
import { LoginClientComponent } from './components/client/login-client/login-client.component';
import { LoginGuardService } from './services/login-guard.service';
import { LoginAdminComponent } from './components/admin/login-admin/login-admin.component';
import { LoginGuardAdminService } from './services/login-guard-admin.service';
import { LoginGuardClientService } from './services/login-guard-client.service';

const clientroutes: Routes = [ 
  { path: "test", redirectTo: "/login", pathMatch: "full" },
  { path: "login", component: LoginComponent },
  { path: "home", component: HomeComponent },
  { path: "user/signup", component: SignupComponent },
  { path: "user/login", component: LoginClientComponent },
  {
    path: 'user', component: ClientUserLayoutComponent,
    canActivate:[LoginGuardClientService],
    children: [
      { path: '', loadChildren: () => import('./components/client/client-user-side.module').then(m => m.ClientUserSideModule) },
    ] 
  },
  {
    path: '', component: ClientLayoutComponent,
    children: [
      { path: '', loadChildren: () => import('./components/client/client-side.module').then(m => m.ClientSideModule) },
    ] 
  },
  
];

const boroutes: Routes = [
  {
    path: 'admin', component: AppLayoutComponent,
    data: { userType: 'admin' }, 
    canActivate:[LoginGuardAdminService],
    children: [
      { path: '', loadChildren: () => import('./components/admin/admin-side.module').then(m => m.AdminSideModule) },
      
      // demo 
      { path: 'demo', loadChildren: () => import('./components/bocomponents/bocomponents.module').then(m => m.BocomponentsModule) },
      { path: 'dashboard', loadChildren: () => import('./components/bocomponents/dashboard/dashboard.module').then(m => m.DashboardModule) },
      
      
    ]
  },
  { path: "admin/login", component: LoginAdminComponent }, 
  {
    path: 'employe', component: AppLayoutComponent,
    data: { userType: 'emp' },
    canActivate:[LoginGuardService],
    children: [
      { path: '', loadChildren: () => import('./components/employe/employe-side.module').then(m => m.EmployeSideModule) }
    ]
  },
  { path: "employe/login", component: LoginEmployeComponent }, 
  { path: "employe/code-pswrd/:id", component: CodePasswordComponent },  
  { path: "employe/edit-pswrd/:id",
   component: EditPasswordComponent },
  
  { path: 'notfound', component: NotfoundComponent },
  { path: '**', redirectTo: '/notfound' },

]; 

@NgModule({
  imports: [    
    RouterModule.forRoot(clientroutes),
     RouterModule.forRoot(boroutes , { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled', onSameUrlNavigation: 'reload' }),
     
    ],    
  exports: [RouterModule]
})
export class AppRoutingModule { }
