// src/environments/environment.prod.ts
export const environment = {
    production: true,
    domain: 'https://m1p10mean-orlando-melodie.onrender.com/'
    // Add other production-specific configuration here
  };
  